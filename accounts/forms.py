from crispy_forms.helper import FormHelper
from crispy_forms_foundation.layout import (AccordionHolder, AccordionItem, Fieldset, Field, ButtonHolder, Column,
                                            Layout, Row, Submit, InlineSwitchField)
from dal import autocomplete
from django.contrib.auth import get_user_model
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm, PasswordResetForm
from django.contrib.gis import forms
from django.forms import CharField
from django.forms.widgets import HiddenInput
from django.utils.translation import ugettext_lazy as _
from leaflet.forms.widgets import LeafletWidget

from .models import Profile


class UserCreateAdminForm(UserCreationForm):
    username = CharField(initial='tmpusername123')
    password_clear = CharField()

    # password_clear = CharField(
    #     max_length=50,
    #     label='Mot de passe',
    #     help_text=_('Le mot de passe prérempli est un mot de passe fort aléatoirement généré'),
    #     initial=BaseUserManager.make_random_password(12),
    # )

    class Meta:
        model = Profile
        fields = UserCreationForm.Meta.fields + (
            'first_name', 'last_name', 'email', 'is_resp', 'resp_territory', 'geom', 'access_all_data',
            'edit_all_data', 'catchauth',
            'organism', 'mobile_phone', 'home_phone', 'addr_appt', 'addr_building', 'addr_street', 'addr_city',
            'addr_city_code',
            'addr_dept', 'addr_country',
            'comment', 'id_bdsource', 'bdsource', 'geom')
        widgets = {'username': HiddenInput(),
                   'password1': HiddenInput(), 'password2': HiddenInput(),
                   'geom': LeafletWidget(),
                   'resp_territory': autocomplete.ModelSelect2Multiple(url='api:territory_autocomplete'), }
        # readonly_fields = ('password_gen',)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem('Informations principales',
                              Fieldset(_("Caractéristiques principales"),
                                       Row(
                                           Column('email',
                                                  css_class='small-12 large-6'),
                                           Column('organism',
                                                  css_class='small-12 large-6'),
                                           Column('first_name',
                                                  css_class='small-12 large-6'),
                                           Column('last_name',
                                                  css_class='small-12 large-6'),
                                       ),
                                       Row(
                                           Field('username', type="hidden"),
                                           Field('password1', type='hidden'),
                                           Field('password2', type='hidden'),
                                           Field('password_clear', type='hidden'),
                                       ),
                                       ),
                              Fieldset(_("Responsabilité et autorisations"),
                                       Row(
                                           Column(
                                               InlineSwitchField('is_resp', label_column='large-12',
                                                                 input_column='large-12'),
                                               css_class='small-12 medium-3 large-2'
                                           ),
                                           Column(
                                               'resp_territory', css_class='small-12 medium-9 large-10'),
                                       ),
                                       Row(
                                           Column(InlineSwitchField('access_all_data', label_column='large-12',
                                                                    input_column='large-12'),

                                                  css_class='small-12 large-6'),
                                           Column(InlineSwitchField('edit_all_data', label_column='large-12',
                                                                    input_column='large-12'),
                                                  css_class='small-12 large-6'),
                                           Column('catchauth', css_class='large-12'),
                                       ),
                                       ),
                              ),
                AccordionItem(_('Coordonnées'),
                              Fieldset(_('Numéros de téléphone'),
                                       Column('mobile_phone', css_class='large-6'),
                                       Column('home_phone', css_class='large-6'),
                                       ),
                              Fieldset(_('Adresse postale'),
                                       Column('addr_appt', css_class='large-2 medium-3 small-6'),
                                       Column('addr_building', css_class='large-2 medium-3 small-6'),
                                       Column('addr_street', css_class='large-8 medium-6 small-12'),
                                       Column('addr_city', css_class='large-6 medium-12'),
                                       Column('addr_city_code', css_class='large-3 medium-12'),
                                       Column('addr_dept', css_class='large-3 medium-12'),
                                       Column('addr_country', css_class='large-12'),
                                       ),
                              Row(
                                  Column('geom', css_class='large-12'),
                              ), ),
                AccordionItem(
                    'Source',
                    Row(
                        Column('bdsource', css_class='medium-6 small-12',
                               readonly=True),
                        Column('id_bdsource', readonly=True,
                               css_class='medium-6 small-12')
                    ),
                ),
                AccordionItem(
                    'Commentaire',
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )
        self.helper.form_show_errors = True

        super(UserCreateAdminForm, self).__init__(*args, **kwargs)


class UserCreateForm(UserCreationForm):
    username = CharField(initial='tmpusername123')
    password_clear = CharField()

    # password_clear = CharField(
    #     max_length=50,
    #     label='Mot de passe',
    #     help_text=_('Le mot de passe prérempli est un mot de passe fort aléatoirement généré'),
    #     initial=BaseUserManager.make_random_password(12),
    # )

    class Meta:
        model = Profile
        fields = UserCreationForm.Meta.fields + (
            'first_name', 'last_name', 'email', 'geom', 'catchauth',
            'organism', 'mobile_phone', 'home_phone', 'addr_appt', 'addr_building', 'addr_street', 'addr_city',
            'addr_city_code',
            'addr_dept', 'addr_country',
            'comment', 'id_bdsource', 'bdsource', 'geom')
        widgets = {'username': HiddenInput(),
                   'password1': HiddenInput(), 'password2': HiddenInput(),
                   'geom': LeafletWidget(),
                   'resp_territory': autocomplete.ModelSelect2Multiple(url='api:territory_autocomplete'), }
        # readonly_fields = ('password_gen',)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem('Informations principales',
                              Fieldset(_("Caractéristiques principales"),
                                       Row(
                                           Column('email',
                                                  css_class='small-12 large-6'),
                                           Column('organism',
                                                  css_class='small-12 large-6'),
                                           Column('first_name',
                                                  css_class='small-12 large-6'),
                                           Column('last_name',
                                                  css_class='small-12 large-6'),
                                       ),
                                       Row(
                                           Field('username', type="hidden"),
                                           Field('password1', type='hidden'),
                                           Field('password2', type='hidden'),
                                           Field('password_clear', type='hidden'),
                                       ),
                                       ),
                              Fieldset(_("Autorisations"),
                                       Row(
                                           Column('catchauth', css_class='large-12'),
                                       ),
                                       ),
                              ),
                AccordionItem(_('Coordonnées'),
                              Fieldset(_('Numéros de téléphone'),
                                       Column('mobile_phone', css_class='large-6'),
                                       Column('home_phone', css_class='large-6'),
                                       ),
                              Fieldset(_('Adresse postale'),
                                       Column('addr_appt', css_class='large-2 medium-3 small-6'),
                                       Column('addr_building', css_class='large-2 medium-3 small-6'),
                                       Column('addr_street', css_class='large-8 medium-6 small-12'),
                                       Column('addr_city', css_class='large-6 medium-12'),
                                       Column('addr_city_code', css_class='large-3 medium-12'),
                                       Column('addr_dept', css_class='large-3 medium-12'),
                                       Column('addr_country', css_class='large-12'),
                                       ),
                              Row(
                                  Column('geom', css_class='large-12'),
                              ), ),
                AccordionItem(
                    'Source',
                    Row(
                        Column('bdsource', css_class='medium-6 small-12',
                               readonly=True),
                        Column('id_bdsource', readonly=True,
                               css_class='medium-6 small-12')
                    ),
                ),
                AccordionItem(
                    'Commentaire',
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )
        self.helper.form_show_errors = True

        super(UserCreateForm, self).__init__(*args, **kwargs)


# class PasswordUpdateForm(PasswordChangeForm):
#
#     def __init__(self, *args, **kwargs):
#         self.helper = FormHelper()
#         # self.helper.form_method = 'post'
#         self.helper.layout = Layout(
#             Row(
#                 Column(
#                     'old_password', css_class='small-12 medium-12 large-4'),
#                 Column('new_password1',
#                        css_class='small-12 medium-6 large-4'),
#                 Column('new_password1',
#                        css_class='small-12 medium-6 large-4'),
#             ),
#             ButtonHolder(
#                 Submit('submit', 'Enregistrer', css_class='button large'),
#             ),
#         )
#
#         self.helper.form_action = 'submit'
#         self.helper.form_show_errors = True
#
#         super(PasswordUpdateForm, self).__init__(*args, **kwargs)


class UserUpdateAdminForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = UserCreationForm.Meta.fields + (
            'first_name', 'last_name', 'email', 'is_resp', 'resp_territory', 'geom', 'access_all_data',
            'edit_all_data', 'catchauth', 'mobile_phone', 'home_phone', 'addr_appt', 'addr_building', 'addr_street',
            'addr_city', 'addr_city_code',
            'addr_dept', 'addr_country', 'comment', 'id_bdsource', 'bdsource', 'geom')
        widgets = {'geom': LeafletWidget(),
                   'resp_territory': autocomplete.ModelSelect2Multiple(url='api:territory_autocomplete'), }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem('Informations principales',
                              Fieldset(_("Caractéristiques principales"),
                                       Row(
                                           Column('email',
                                                  css_class='small-12 medium-12 large-4'),
                                           Column('first_name',
                                                  css_class='small-12 medium-6 large-4'),
                                           Column('last_name',
                                                  css_class='small-12 medium-6 large-4'),
                                       ),
                                       Row(
                                           Column('username', css_class='large-12'),
                                       ),
                                       ),
                              Fieldset(_("Responsabilité et autorisations"),
                                       Row(
                                           Column(
                                               InlineSwitchField('is_resp', label_column='large-12',
                                                                 input_column='large-12'),
                                               css_class='small-12 medium-3 large-2'
                                           ),
                                           Column(
                                               'resp_territory', css_class='small-12 medium-9 large-10'),
                                       ),
                                       Row(
                                           Column(InlineSwitchField('access_all_data', label_column='large-12',
                                                                    input_column='large-12'),

                                                  css_class='small-12 large-6'),
                                           Column(InlineSwitchField('edit_all_data', label_column='large-12',
                                                                    input_column='large-12'),
                                                  css_class='small-12 large-6'),
                                           Column('catchauth', css_class='large-12'),

                                       ),
                                       ),
                              ),
                AccordionItem(_('Coordonnées'),
                              Fieldset(_('Numéros de téléphone'),
                                       Column('mobile_phone', css_class='large-6'),
                                       Column('home_phone', css_class='large-6'),
                                       ),
                              Fieldset(_('Adresse postale'),
                                       Column('addr_appt', css_class='large-2 medium-3 small-6'),
                                       Column('addr_building', css_class='large-2 medium-3 small-6'),
                                       Column('addr_street', css_class='large-8 medium-6 small-12'),
                                       Column('addr_city', css_class='large-6 medium-12'),
                                       Column('addr_city_code', css_class='large-3 medium-12'),
                                       Column('addr_dept', css_class='large-3 medium-12'),
                                       Column('addr_country', css_class='large-12'),
                                       ),
                              Row(
                                  Column('geom', css_class='large-12'),
                              ), ),
                AccordionItem(
                    'Source',
                    Row(
                        Column('bdsource', css_class='medium-6 small-12',
                               readonly=True),
                        Column('id_bdsource', readonly=True,
                               css_class='medium-6 small-12')
                    ),
                ),
                AccordionItem(
                    'Commentaire',
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )
        self.helper.form_action = 'submit'
        self.helper.form_show_errors = True

        super(UserUpdateAdminForm, self).__init__(*args, **kwargs)


class UserAdminUpdatePasswordForm(forms.ModelForm):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.PasswordInput,
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    new_password2 = forms.CharField(
        label=_("New password confirmation"),
        strip=False,
        widget=forms.PasswordInput,
    )

    class Meta:
        model = Profile
        fields = ('new_password1', 'new_password2')
        readonly_fields = ('password_generated',)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            Row(
                Column('new_password1', css_class='large-6 small-12'),
                Column('new_password2', css_class='large-6 small-12'),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )
        super(UserAdminUpdatePasswordForm, self).__init__(*args, **kwargs)

    #
    def clean_new_password2(self, **kwargs):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        password_validation.validate_password(password2)
        return password2

    def save(self, commit=True, **kwargs):
        password = self.cleaned_data["new_password1"]
        username = self.instance.username
        user = get_user_model().objects.get(username=username)
        user.set_password(password)
        user.is_active = True
        if commit:
            user.save()
        return user


class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('first_name', 'last_name', 'email', 'catchauth',
                  'mobile_phone', 'home_phone', 'addr_appt', 'addr_building', 'addr_street', 'addr_city',
                  'addr_city_code',
                  'addr_dept', 'addr_country',
                  'comment', 'geom')
        widgets = {'geom': LeafletWidget(),
                   'resp_territory': autocomplete.ModelSelect2Multiple(url='api:territory_autocomplete'), }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem('Informations principales',
                              Fieldset(_("Caractéristiques principales"),
                                       Row(
                                           Column('email',
                                                  css_class='small-12 medium-12 large-4'),
                                           Column('first_name',
                                                  css_class='small-12 medium-6 large-4'),
                                           Column('last_name',
                                                  css_class='small-12 medium-6 large-4'),
                                           Field('username', type="hidden"),
                                       ),
                                       ),
                              Fieldset(_("Responsabilité et autorisations"),
                                       Row(
                                           Column(
                                               InlineSwitchField('is_resp', label_column='large-12',
                                                                 input_column='large-12'),
                                               css_class='small-12 medium-3 large-2'
                                           ),
                                           Column(
                                               'resp_territory', css_class='small-12 medium-9 large-10'),
                                       ),
                                       Row(
                                           Column(InlineSwitchField('access_all_data', label_column='large-12',
                                                                    input_column='large-12'),

                                                  css_class='small-12 large-6'),
                                           Column(InlineSwitchField('edit_all_data', label_column='large-12',
                                                                    input_column='large-12'),
                                                  css_class='small-12 large-6'),
                                           Column('catchauth', css_class='large-12'),

                                       ),
                                       ),
                              ),
                AccordionItem(_('Coordonnées'),
                              Fieldset(_('Numéros de téléphone'),
                                       Column('mobile_phone', css_class='large-6'),
                                       Column('home_phone', css_class='large-6'),
                                       ),
                              Fieldset(_('Adresse postale'),
                                       Column('addr_appt', css_class='large-2 medium-3 small-6'),
                                       Column('addr_building', css_class='large-2 medium-3 small-6'),
                                       Column('addr_street', css_class='large-8 medium-6 small-12'),
                                       Column('addr_city', css_class='large-6 medium-12'),
                                       Column('addr_city_code', css_class='large-3 medium-12'),
                                       Column('addr_dept', css_class='large-3 medium-12'),
                                       Column('addr_country', css_class='large-12'),
                                       ),
                              Fieldset(_('Carte de localisation'),
                                       Row(
                                           Column('geom', css_class='large-12'),
                                       ), ), ),
                AccordionItem(
                    'Commentaire',
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )
        self.helper.form_action = 'submit'
        self.helper.form_show_errors = True

        super(UserUpdateForm, self).__init__(*args, **kwargs)

        def clean_password(self):
            return self.initial['password']


class PasswordUpdateForm(PasswordChangeForm):
    # this is now only needed for styling purposes
    helper = FormHelper()
    helper.form_tag = False
    helper.label_class = 'medium-5'
    helper.field_class = 'medium-7'
    helper.layout = Layout('old_password', 'new_password1', 'new_password2',
                           ButtonHolder(
                               Submit('submit', 'Enregistrer', css_class='button large'), css_class='text-right'
                           ),
                           )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(PasswordUpdateForm, self).__init__(user, *args, **kwargs)


# # ensures correct ordering of form fields
# PasswordUpdateForm.base_fields = OrderedDict(
#     (k, PasswordUpdateForm.base_fields[k])
#     for k in ['old_password', 'new_password1', 'new_password2']
# )


class PasswordResetForm(PasswordResetForm):
    def __init__(self, *args, **kw):
        super(PasswordResetFormExtra, self).__init__(*args, **kw)

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'small-12 medium-6 large-4'
        self.helper.field_class = 'small-12 medium-6 large-8'
        self.helper.layout = Layout(
            'email',
            Div(
                Submit('submit', 'Réinitialiser le mot de passe', css_class='button'),
                HTML('<a class="button alert" href="/">Annuler</a>'),
                css_class='text-right',
            )
        )
