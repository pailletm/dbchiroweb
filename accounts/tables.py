import django_tables2 as tables
from django.utils.translation import ugettext_lazy as _
from django_tables2.utils import A

from .models import Profile


class ProfileTable(tables.Table):
    ACTIONS = """
        <a href="{% url 'accounts:user_detail' pk=record.pk %}" title="Voir les informations"><i class="fi-eye"></i></a>
        <a href="{% url 'accounts:user_update' pk=record.pk %}" title="Modifier"><i class="fi-page-edit"></i></a>
        <a href="{% url 'accounts:user_password' pk=record.pk %}" title="Modifier le mot de passe"><i class="fi-key"></i></a>
        <a href="{% url 'sights:user_sighting_list' pk=record.pk %}" title="Voir ses observations"><i class="fi-list"></i></a>
        <a href="{% url 'accounts:user_delete' pk=record.pk %}" title="Supprimer"><i class="fi-trash"></i></a>
        """
    MODIFY_BY = """
    {% if record.updated_by %}{{record.updated_by}}{% else %}{{ record.created_by }}{% endif %}
    """
    is_resp = tables.BooleanColumn(verbose_name=_('Responsable local'))
    access_all_data = tables.BooleanColumn(verbose_name=_('Peut tout éditer'))
    edit_all_data = tables.BooleanColumn(verbose_name=_('Peut tout lire'))
    mobile_phone = tables.Column(verbose_name=_('n° mobile'))
    home_phone = tables.Column(verbose_name=_('n° domicile'))
    email = tables.EmailColumn(verbose_name=_('@email'))
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})
    username = tables.LinkColumn('accounts:user_detail', args=[A('id')], accessor='username')
    modify_by = tables.TemplateColumn(
        MODIFY_BY, verbose_name="Dernière modification", orderable=False)
    last_login = tables.DateTimeColumn()

    class Meta:
        model = Profile
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = (
            'actions', 'username', 'email', 'first_name', 'last_name', 'organism', 'is_active', 'is_resp',
            'access_all_data',
            'edit_all_data', 'home_phone', 'mobile_phone', 'addr_city', 'last_login', 'modify_by',)
        exclude = ('id',)
