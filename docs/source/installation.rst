.. _`installation`:

************
Installation
************

L'installationd de dbChiro[web] a été uniquement testée sur serveur Debian Jessie en production, Debian Stretch en développement. Ce tutoriel d'installation ne traitera donc que de ces distributions.

**Prerequis**:

* Un serveur Debian Jessie ou Debian Stretch 64 bits
* Un émulateur de terminal pour la connexion via ssh (natif sous Linux, `Putty <http://www.chiark.greenend.org.uk/~sgtatham/putty/>`_ ou `mobaXterm <https://mobaxterm.mobatek.net/>`_ sous windows)

Installation de PostgreSQL et de PostGIS
========================================
Dans un terminal, en tant que superutilisateur, lancez les commandes suivantes

.. code-block:: sh

    sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
    sudo apt-get install wget ca-certificates
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get install git python3 python3-pip postgresql-10 postgresql-10-postgis-2.4 postgresql-10-postgis-2.4-scripts


Clonage du dépot dbChiro[web]
=============================

Méthode recommandée pour faciliter les mises à jour.

Dans cet exemple, dbchiroweb sera installé dans le dossier `/var/www/dbchiro`

.. code-block:: sh

    mkdir /var/www/dbchiro
    cd /var/www/dbchiro
    git clone https://framagit.org/dbchiro/dbchiroweb.git

Création de l'environnement virtuel
===================================

Dans cet exemple, dbchiroweb sera installé dans le dossier `/var/www/dbchiro`

.. code-block:: sh

    cd /var/www/dbchiro
    virtualenv -p python3 venv
    source venv/bin/activate

Création de la base de donnée et du superutilisateur de la base de donnée
=========================================================================

.. code-block:: sh

    # en tant que root
    su postgres
    psql

.. code:: psql

    CREATE DATABASE dbchirodb;
    CREATE ROLE dbchiropguser LOGIN SUPERUSER ENCRYPTED PASSWORD 'motdepasse';
    ALTER DATABASE dbchirodb OWNER TO dbchiropguser;
    -- Se connecter à la base de donnée avec la commande \c
    \c dbchirodb
    CREATE EXTENSION postgis;
    -- Quitter psql
    \q


Configuration de dbChiro[web]
=============================

Copiez le fichier ``dbchiro/settings/configuration/config.py.sample`` en le renommant ``config.py`` dans ce même dossier

.. code-block:: sh

    cd /var/www/dbchiro/dbchiroweb
    cp dbchiro/settings/configuration/config.py.sample dbchiro/settings/configuration/config.py
    nano bchiro/settings/configuration/config.py


.. literalinclude:: ../../dbchiro/settings/configuration/config.py.sample
    :language: python


**La répartition des périodes est la suivante**

+-----------------------------------------+----------+-------------+-----------+----------+
|Jour de l'année                          | 335      | 60          | 136       | 228      |
+-----------------------------------------+----------+-------------+-----------+----------+
|date                                     | 01/12    | 01/03       | 16/05     | 16/08    |
+=========================================+==========+=============+===========+==========+
|Hivernage / wintering (w)                |     ≥    |     <       |           |          |
+-----------------------------------------+----------+-------------+-----------+----------+
|Transit printanier / sping transit (st)  |          |     ≥       |     <     |          |
+-----------------------------------------+----------+-------------+-----------+----------+
|Estivage / Summering (e)                 |          |             |     ≥     |    <     |
+-----------------------------------------+----------+-------------+-----------+----------+
|Transit automnal / Automn transit (ta)   |     <    |             |     ≥     |          |
+-----------------------------------------+----------+-------------+-----------+----------+


Initialisation de la base de données
====================================

Pour ce projet, deux configurations de ``settings`` sont possibles:

* ``production`` pour le serveur en production
* ``dev`` pour le développement. Avec notamment le mode de débogage et l'appli django-debug-toolbar

Les deux peuvent être utilisés pour les commandes suivantes. Cette exemple utilisera la configuration ``dev``.

.. code-block:: sh

    cd /var/www/dbchiro/dbchiroweb
    source ../venv/bin/activate
    # Création des fichiers d'initialisation de la base de donnée
    python manage.py makemigrations --settings=dbchiro.settings.dev accounts blog dicts geodata management sights
    # Création de la structure de la base de donnée (à partir des fichiers créées la commande
    python manage.py migrate --settings=dbchiro.settings.dev
    # Création du dossier 'static' et importation des fichiers nécessaire au fonctionnement de l'interface graphique (css, js, polices).
    python manage.py migrate --settings=dbchiro.settings.dev
    # Création du premier utilisateur, administrateur du site
    python manage.py createsuperuser --settings=dbchiro.settings.dev
    # Lancement du serveur
    python manage.py runserver --settings=dbchiro.settings.dev

Le terminal devrait alors renvoyer le résultat suivant:

.. code::

    Performing system checks...

    System check identified no issues (0 silenced).
    January 17, 2018 - 22:29:11
    Django version 1.11.3, using settings 'dbchiro.settings.dev'
    Starting development server at http://127.0.0.1:8000/
    Quit the server with CONTROL-C.

Vous pouvez donc tester l'application à cette adresse : http://127.0.0.1:8000/

Pour la tester à distance, il faut utiliser un tunnel ssh entre la machine distante et la machine locale.
Sur Linux ou osX, cela se fait tout simplement avec la commande suivante (8000 est le port par défaut de django):

.. code-block:: sh

    ssh -p portssh utilisateurserver@adresseserver -L 8000:localhost:8000

Depuis Windows, cela peut se faire avec Putty: http://www.eila.univ-paris-diderot.fr/sysadmin/windows/tunnels-putty

Intégration des données initiales (dictionnaires et geodata)
============================================================

L'étape suivante consiste à intégrer à la base de donnée les principales données nécessaires à son fonctionnemnt:
* les dictionnaires (application `dicts`)
* les données géographiques (application `geodata`)

Intégration des données de dictionnaire
---------------------------------------

Exécution du fichier `initial_data/sql/dicts_french.sql` avec la commande suivante:

.. code-block:: sh

    psql -h localhost -p 5432 -U dbchiropguser -W -f initial_data/sql/dicts_french.sql dbchirodb

Intégration des données cartographiques
---------------------------------------
Les données fournies sont en projection Lambert93 (epsg 2154). Il convient de les adapter à la projection de la base de donnée. Cela est réalisé par la commande d'importation suivante.
Plus d'informations sur cette commande ici: http://bostongis.com/pgsql2shp_shp2pgsql_quickguide.bqg

.. code-block:: sh

    shp2pgsql -s 2154:4326 -a initial_data/shapefiles/territory.shp public.geodata_territory | psql -h localhost -U dbchiro  dbchirodb
    shp2pgsql -s 2154:4326 -a initial_data/shapefiles/municipality.shp public.geodata_municipality | psql -h localhost -p 5432 -U dbchiro  dbchirodb

Configuration du serveur apache
===============================

crééez le fichier de configuration du serveur apache. Cela par du principe que le domaine, ou sous-domaine, dispose d'une redirection vers l'adresse IP du présent serveur.
Plus d'informations à cette adresse: https://docs.djangoproject.com/fr/1.11/howto/deployment/wsgi/modwsgi/

.. code-block:: sh

    # Activation du module wsgi
    a2enmod wsgi
    # Création du fichier de configuration apache pour dbChiro
    nano /etc/apache2/sites-avalaible/dbchiro.conf

Copiez-y le code suivant et adaptez le

.. code:: apache

    <VirtualHost mondomaine.org:80>
        ServerName mondomaine.org
        ServerAlias www.mondomaine.org
        ServerAdmin admin@dbchiro.org
        Alias /static /var/www/dbchiro/dbchiroweb/dbchiro/static
        WSGIDaemonProcess dbchiro python-home=/var/www/dbchiro/venv python-path=/var/www/dbchiro/dbchiroweb
        WSGIProcessGroup dbchiro
        WSGIScriptAlias / /var/www/dbchiro/dbchiroweb/dbchiro/wsgi.py
        <Directory /var/www/dbchiro/dbchiroweb/dbchiro>
            <files wsgi.py>
                Require all granted
            </files>
        </Directory>
        CustomLog /var/www/dbchiro/custom.log combined
        ErrorLog /var/www/dbchiro/error.log
    </VirtualHost>

Activez cette configuration avec la commande suivante:

.. code-block:: sh

    a2ensite dbchiro.conf
    service apache2 restart

Vous devriez maintenant accéder à votre plateforme depuis le nom de domaine renseigné. connectez vous avec votre compte superuser précédemment créé.