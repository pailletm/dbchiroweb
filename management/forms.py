from crispy_forms.helper import FormHelper
from crispy_forms_foundation.layout import (AccordionHolder, AccordionItem, ButtonHolder, Column, Layout, Row, Submit,
                                            Callout, HTML)
from dal import autocomplete
from django import forms

from .models import Study, Transmitter, CatchAuth


class StudyForm(forms.ModelForm):
    class Meta:
        model = Study
        fields = ('name', 'year', 'project_manager', 'public_funding', 'public_report',
                  'public_raw_data', 'confidential', 'confidential_end_date',
                  'type_etude', 'type_espace', 'comment')

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        # ======================================================================
        # self.fields['transmitter'].queryset = Transmitter.objects.filter(
        #     available is True)
        # ======================================================================
        self.helper.layout = Layout(AccordionHolder(
            AccordionItem('Elements principaux de l\'étude',
                          Row(
                              Column(
                                  Callout(
                                      Row(
                                          Column(HTML('<h5>Nom et année de réalisation</h5>'),
                                                 css_class='large-12'),
                                          Column(
                                              'name', css_class='large-4 small-12'),
                                          Column(
                                              'year', css_class='large-4 small-6'),
                                          Column(
                                              'project_manager', css_class='large-4 small-6'),
                                          Column(
                                              'type_etude', css_class='large-6 small-12'),
                                          Column(
                                              'type_espace', css_class='large-6 small-12'),
                                      ), css_class='secondary'),
                                  css_class='large-12'),
                              Column(
                                  Callout(
                                      Row(
                                          Column(HTML('<h5>Caractère publique et confidentialité</h5>'),
                                                 css_class='large-12'),
                                          Column(
                                              'public_funding', css_class='large-4 small-6'),
                                          Column(
                                              'public_report', css_class='large-4 small-6'),
                                          Column(
                                              'public_raw_data', css_class='large-4 small-6'),
                                          Column(
                                              'confidential', css_class='large-6 small-6'),
                                          Column(
                                              'confidential_end_date', css_class='large-6 small-6'),
                                      ), css_class='secondary'),
                                  css_class='large-12'), ),
                          ),
            AccordionItem(
                'Commentaire',
                Row(
                    Column('comment', css_class='large-12'),
                ),
            ),
        ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )

        super(StudyForm, self).__init__(*args, **kwargs)


class TransmitterForm(forms.ModelForm):
    class Meta:
        model = Transmitter
        fields = ('reference', 'frequency', 'available', 'weight', 'autonomy', 'model', 'brand',
                  'owner', 'buying_date', 'last_recond_date', 'status', 'comment')

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            Row(Column('available',
                       css_class='large-6'),
                Column(
                    ButtonHolder(Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'),
                    css_class='large-6'),
                ),
            AccordionHolder(
                AccordionItem('Informations principales',
                              Row(
                                  Column('reference',
                                         css_class='large-4 medium-6 small-12'),
                                  Column('frequency',
                                         css_class='large-4 medium-6 small-12'),
                                  Column('weight',
                                         css_class='large-4 medium-6 small-12'),
                                  Column('autonomy',
                                         css_class='large-4 medium-6 small-12'),
                                  Column('model',
                                         css_class='large-4 medium-6 small-12'),
                                  Column('brand',
                                         css_class='large-4 medium-6 small-12'),
                                  Column('buying_date',
                                         css_class='large-6 medium-6 small-12'),
                                  Column('last_recond_date',
                                         css_class='large-6 medium-6 small-12'), ),
                              Row(
                                  Column('owner',
                                         css_class='medium-6 small-12'),
                                  Column('status',
                                         css_class='medium-6 small-12'),
                              ), ),
                AccordionItem('Commentaire',
                              Row(
                                  Column('comment',
                                         css_class='large-12'),
                              ),
                              ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )

        super(TransmitterForm, self).__init__(*args, **kwargs)


class CatchAuthForm(forms.ModelForm):
    class Meta:
        model = CatchAuth
        fields = ('territory', 'date_start', 'date_end', 'official_reference', 'file')
        widgets = {'territory': autocomplete.ModelSelect2Multiple(url='api:territory_autocomplete'), }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        # ======================================================================
        # self.fields['transmitter'].queryset = Transmitter.objects.filter(
        #     available is True)
        # ======================================================================
        self.helper.layout = Layout(AccordionHolder(
            AccordionItem('Données de l\'arrêté',
                          Row(
                              Column('territory', css_class='large-6 small-12'),
                              Column('official_reference', css_class='large-6 small-12'),
                              Column('date_start', css_class='large-6 small-12'),
                              Column('date_end', css_class='large-6 small-12'),
                          ), ),
            AccordionItem(
                'Fichier',
                Row(
                    Column('file', css_class='large-12'),
                ),
            ),
        ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )

        super(CatchAuthForm, self).__init__(*args, **kwargs)
