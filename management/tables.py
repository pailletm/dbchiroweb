import django_tables2 as tables

from .models import Study, Transmitter, CatchAuth


class StudyTable(tables.Table):
    ACTIONS = '''
        <a data-open="StudyTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
    	<div class="reveal" id="StudyTableHistoryModal{{ record.pk }}" data-reveal>
    		    <h1>Historique de la donnée</h1>
    			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
    				{% if t.timestamp_update %}
    				et mise à jour le {{ record.timestamp_update }}
    				par {{ record.updated_by }}
    				{% endif %}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div> 
        <a href="{% url 'management:study_update' record.pk %}" title="Modifier"><i class="fi-page-edit"></i></a>
        <a href="{% url 'management:study_delete' record.pk %}" title="Supprimer"><i class="fi-trash"></i></a>
        '''
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False)

    class Meta:
        model = Study
        attrs = {'class': 'responsive'}
        fields = ('actions', 'name', 'year', 'project_manager', 'type_etude', 'type_espace',
                  'public_funding', 'public_report', 'public_raw_data', 'confidential', 'confidential_end_data',
                  'timestamp_create', 'timestamp_update', 'created_by', 'comment')


class TransmitterTable(tables.Table):
    ACTIONS = '''
        <a data-open="SessionTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
    		<div class="reveal" id="SessionTableHistoryModal{{ record.pk }}" data-reveal>
    		    <h1>Historique de la donnée</h1>
    			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
    				{% if t.timestamp_update %}
    				et mise à jour le {{ record.timestamp_update }}
    				par {{ record.updated_by }}
    				{% endif %}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div> 
        <a href="{% url 'management:transmitter_update' record.pk %}" title="Modifier"><i class="fi-page-edit"></i></a>
        <a href="{% url 'management:transmitter_delete' record.pk %}" title="Supprimer"><i class="fi-trash"></i></a>
        '''
    COMMENT = '''
            {% if record.comment %}
            <a data-open="TransmitterCommentModal{{ record.pk }}" title="Commentaire"><i class="fi-comments"></i></a>
        		<div class="reveal" id="TransmitterCommentModal{{ record.pk }}" data-reveal>
    		    <h1>Commentaire</h1>
    			    <p>{{ record.comment }}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>
            {% endif %}
        '''
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False)
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = Transmitter
        attrs = {'class': 'responsive'}
        fields = ('actions', 'name', 'reference', 'frequency', 'weight', 'autonomy', 'model', 'brand', 'owner',
                  'buying_date', 'last_recond_date', 'status', 'available', 'comment')


class CatchAuthTable(tables.Table):
    ACTIONS = '''
        <a data-open="SessionTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
    		<div class="reveal" id="SessionTableHistoryModal{{ record.pk }}" data-reveal>
    		    <h1>Historique de la donnée</h1>
    			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
    				{% if t.timestamp_update %}
    				et mise à jour le {{ record.timestamp_update }}
    				par {{ record.updated_by }}
    				{% endif %}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div> 
        <a href="{% url 'management:catchauth_update' record.pk %}" title="Modifier"><i class="fi-page-edit"></i></a>
        <a href="{% url 'management:catchauth_delete' record.pk %}" title="Supprimer"><i class="fi-trash"></i></a>
        '''
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False)

    class Meta:
        model = CatchAuth
        attrs = {'class': 'responsive'}
        fields = ('actions', 'territory', 'official_reference', 'date_start', 'date_end', 'file')
