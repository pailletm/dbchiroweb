from dbchiro.settings.base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

DEV_APPS = [
    'debug_toolbar',
]

INSTALLED_APPS = MAIN_APPS + DEV_APPS + PROJECT_APPS
