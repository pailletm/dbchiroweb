from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _

from django.db import models

# Create your models here.
class TypePlace(models.Model):
    """
    Catégories des types localité utilisé dans la relation :model:`sights.Place`.
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    category = models.CharField(max_length=30)
    descr = models.CharField(max_length=200)

    def __str__(self):
        return "%s ∙ %s ∙ %s" % (self.code, self.category, self.descr)

    class Meta:
        verbose_name_plural = "Catégories des types de localités"
        ordering = ['category', 'code']


class PlacePrecision(models.Model):
    """
    Catégories des précisions géographiques utilisé dans la relation :model:`sights.Place`.
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=15)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return "%s ∙ %s" % (self.code, self.descr)

    class Meta:
        verbose_name_plural = "Catégories des types de précisions des localités"


class State(models.Model):
    """
    Etat des gîtes
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=15)

    def __str__(self):
        return self.code

    class Meta:
        verbose_name_plural = "Etat des gîtes"


class Interest(models.Model):
    """
    Intérêt des gîtes
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=15)

    def __str__(self):
        return self.code

    class Meta:
        verbose_name_plural = "Intérêt des gîtes"


class PlaceBatiDetail(models.Model):
    """
    Méthodes d'inventaire/d'observations :model:`sights.Sighting`.
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return "%s ∙ %s" % (self.code, self.descr)

    class Meta:
        verbose_name_plural = "Type de bâti"

class GiteBatAccess(models.Model):
    """
    Accessibilité des éléments des gites aux chiros
    """
    id = models.IntegerField(primary_key=True)
    descr = models.CharField(max_length=20)

    def __str__(self):
        return self.descr

    class Meta:
        verbose_name_plural = "Accessibilité des éléments d'un gîte"

class BuildCover(models.Model):
    """
    Accessibilité des éléments des gites aux chiros
    """
    id = models.IntegerField(primary_key=True)
    descr = models.CharField(max_length=20)

    def __str__(self):
        return self.descr

    class Meta:
        verbose_name_plural = _('Types de couvertures d\'un bâtiment')

class TreeSpecies(models.Model):
    """
    Dionnaire des essences d'arbres
    Pour les bases françaises, l'id de la table correspond au champ cd_nom du référentiel taxonomique TaxRef
    """
    id = models.IntegerField(primary_key=True)
    common_name = models.CharField(max_length=200)
    latin_name = models.CharField(max_length=200)
    full_latin_name = models.CharField(max_length=300)
    tax_level = models.CharField(max_length=50)

    def __str__(self):
        return self.full_latin_name

class TreeHealth(models.Model):
    """
    Etats de santé de l'arbre
    """
    id = models.IntegerField(primary_key=True)
    descr = models.CharField(max_length=20)

    def __str__(self):
        return self.descr

    class Meta:
        verbose_name_plural = _('Etats de santé de l\'arbre')


class TreeContext(models.Model):
    """
    Tree biotop context :model:`sights.PlaceTreeDetail`.
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return self.descr

    class Meta:
        verbose_name_plural = "ionnaire des Contextes écologiques des arbres"


class TreeForestStands(models.Model):
    """
    Forest stands :model:`sights.PlaceTreeDetail`.
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return self.descr

    class Meta:
        verbose_name_plural = "ionnaire des peuplements forestiers"


class TreeCircumstance(models.Model):
    """
    Discover tree circumstances :model:`sights.PlaceTreeDetail`.
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return self.descr

    class Meta:
        verbose_name_plural = "ionnaire des circonstances de la découverte de l\'arbre"


class TreeGiteType(models.Model):
    """
    Tree gite type :model:`sights.PlaceTreeDetail`.
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return self.descr

    class Meta:
        verbose_name_plural = "ionnaire des types de gites arboricoles"


class TreeGiteOrigin(models.Model):
    """
    Tree gite origin :model:`sights.PlaceTreeDetail`.
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return self.descr

    class Meta:
        verbose_name_plural = "Dicionnaire des origines des types de gîtes arboricoles"


class TreeGitePlace(models.Model):
    """
    Tree gite place :model:`sights.PlaceTreeDetail`.
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return self.descr

    class Meta:
        verbose_name_plural = "Dictionnaire des localisations des gîtes arboricoles"


class TreeBecoming(models.Model):
    """
    Tree becoming :model:`sights.PlaceTreeDetail`.
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return self.descr

    class Meta:
        verbose_name_plural = "ionnaire des devenirs des arbres"


class Contact(models.Model):
    """
    ionnaire des types de contacts
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6, verbose_name=_("Code du contact"))
    descr = models.CharField(max_length=50, verbose_name=_("Contact"))

    def __str__(self):
        return "%s ∙ %s" % (self.code, self.descr)

    class Meta:
        verbose_name_plural = "Types de contacts"

class Method(models.Model):
    """
    Méthodes d'inventaire/d'observations :model:`sights.Sighting`.
    """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6, verbose_name="Code de méthode")
    contact = models.CharField(max_length=5, verbose_name="Type de contact")
    descr = models.CharField(max_length=50, verbose_name=_("Méthode"))

    def __str__(self):
        return "%s ∙ %s" % (self.code, self.descr)

    class Meta:
        verbose_name_plural = "Méthodes d'inventaire/d'observations"


class Specie(models.Model):
    id = models.IntegerField(primary_key=True)
    sys_order = models.IntegerField()
    codesp = models.CharField(max_length=20)
    sp_true = models.BooleanField(default=False)
    sci_name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)
    common_name_fr = models.CharField(max_length=255, blank=True, null=True)
    common_name_eng = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return "%s ∙ %s" % (self.codesp, self.common_name_fr)

    class Meta:
        verbose_name_plural = "Liste des codes espèces de la base de données"
        ordering = ['sp_true','sys_order']


class SpecieDetail(models.Model):
    id = models.IntegerField(primary_key=True)
    specie = models.OneToOneField(Specie, on_delete=models.CASCADE)
    sys_order = models.IntegerField()
    codesp = models.CharField(max_length=20),
    sp_fossil = models.BooleanField(default=False),
    sp_detec = models.BooleanField(default=False),
    sp_atlas = models.BooleanField(default=False),
    family = models.CharField(max_length=255, blank=True, null=True)
    author = models.CharField(max_length=255, blank=True, null=True)
    id_n2000 = models.IntegerField(blank=True, null=True)
    id_faunaeuropea = models.IntegerField(blank=True, null=True)
    id_cdref_taxref = models.IntegerField(blank=True, null=True)
    id_inpn2008 = models.IntegerField(blank=True, null=True)
    id_visionature = models.IntegerField(blank=True, null=True)
    id_serena = models.IntegerField(blank=True, null=True)
    id_fromsrc = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return models.Model.__str__(self)

    class Meta:
        verbose_name_plural = "Données détaillées des espèces (correspondances de référentiels, etc.)"


class SpecieStatus(models.Model):
    id = models.IntegerField(primary_key=True)
    specie = models.OneToOneField(Specie, on_delete=models.CASCADE)
    protnat = models.CharField(max_length=10, blank=True, null=True)
    dhff = models.CharField(max_length=10, blank=True, null=True)
    bern_conv = models.CharField(max_length=10, blank=True, null=True)
    bonn_conv = models.CharField(max_length=10, blank=True, null=True)
    bonn_conv_eurobats = models.CharField(max_length=10, blank=True, null=True)
    world_redlist = models.CharField(max_length=10, blank=True, null=True)
    medit_redlist = models.CharField(max_length=10, blank=True, null=True)
    europ_redlist = models.CharField(max_length=10, blank=True, null=True)
    fr_nat_redlist = models.CharField(max_length=10, blank=True, null=True)
    fr_regra_redlist = models.CharField(max_length=10, blank=True, null=True)
    fr_regauv_redlist = models.CharField(max_length=10, blank=True, null=True)

    def __str__(self):
        return models.Model.__str__(self)

    class Meta:
        verbose_name_plural = "Statuts réglementaires des espèces"


class PropertyDomain(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=4)
    domain = models.CharField(max_length=15)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return "%s ∙ %s" % (self.code, self.descr)

    class Meta:
        verbose_name_plural = "Types de propriétés foncières"


class CountPrecision(models.Model):
    id = models.IntegerField(primary_key=True)
    contact = models.CharField(max_length=5, verbose_name=_('type de contacts'), default='0')
    code = models.CharField(max_length=10)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return "%s ∙ %s" % (self.code, self.descr)

    class Meta:
        verbose_name_plural = "Catégories de précisions de comptages"


class TypeDevice(models.Model):
    id = models.IntegerField(primary_key=True)
    contact = models.CharField(max_length=2)
    code = models.CharField(max_length=10)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return self.descr

    class Meta:
        verbose_name_plural = "Types de dispositifs d'échantillonnage"


class CountUnit(models.Model):
    id = models.IntegerField(primary_key=True)
    contact = models.CharField(max_length=5, verbose_name=_('type de contacts'), default='0')
    code = models.CharField(max_length=10)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return "%s ∙ %s" % (self.code, self.descr)

    class Meta:
        verbose_name_plural = "Catégories d'unités de comptages"


class Sex(models.Model):
    """ ionnaire des sexes """
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=2)
    descr = models.CharField(max_length=50, verbose_name=_('Sexe'))

    def __str__(self):
        return "%s ∙ %s" % (self.code, self.descr)

    class Meta:
        verbose_name_plural = "Catégories de sexes"


class Age(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=3)
    descr = models.CharField(max_length=100,verbose_name=_('Age'))

    def __str__(self):
        return "%s ∙ %s" % (self.code, self.descr)

    class Meta:
        verbose_name_plural = "Catégories d'ages"


class Period(models.Model):
    """ ionnaire des périodes """

    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=3)
    descr = models.CharField(max_length=50, verbose_name=_('Période'))
    period = models.CharField(max_length=50)
    day_start = models.IntegerField()
    month_start = models.IntegerField()
    day_end = models.IntegerField()
    month_end = models.IntegerField()

    def __str__(self):
        return "%s ∙ %s du %s/%s au %s/%s" % (
            self.code, self.descr, self.day_start, self.month_start, self.day_end, self.month_end)

    class Meta:
        verbose_name_plural = "Catégories des périodes du cycle annuel"
        ordering = ['id', ]


class BiomTesticule(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=3)
    short_descr = models.CharField(max_length=50, verbose_name=_('Testicule'))
    descr = models.CharField(max_length=100)

    def __str__(self):
        return "%s ∙ %s ∙ %s" % (self.id, self.code, self.short_descr)

    class Meta:
        verbose_name_plural = "Catégories d'état des testicules"


class BiomEpipidyme(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=3)
    short_descr = models.CharField(max_length=50,verbose_name=_('Epididyme'))
    descr = models.CharField(max_length=100)

    def __str__(self):
        return "%s ∙ %s ∙ %s" % (self.id, self.code, self.short_descr)

    class Meta:
        verbose_name_plural = "Catégories d'état des épididymes"


class BiomTuniqVag(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    short_descr = models.CharField(max_length=50)
    descr = models.CharField(max_length=100,verbose_name=_('Tunique vaginale'))

    def __str__(self):
        return "%s ∙ %s ∙ %s" % (self.id, self.code, self.short_descr)

    class Meta:
        verbose_name_plural = "Catégories d'état des tuniques vaginales"


class BiomGlandTaille(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    short_descr = models.CharField(max_length=50, verbose_name=_('Taille des glandes'))
    descr = models.CharField(max_length=100, verbose_name=_('Taille des glandes'))

    def __str__(self):
        return "%s ∙ %s ∙ %s" % (self.id, self.code, self.short_descr)

    class Meta:
        verbose_name_plural = "Catégories de taille des glandes buccales"


class BiomGlandCoul(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    short_descr = models.CharField(max_length=50, verbose_name=_('Couleur des glandes'))
    descr = models.CharField(max_length=100, verbose_name=_('Couleur des glandes'))

    def __str__(self):
        return "%s ∙ %s ∙ %s" % (self.id, self.code, self.short_descr)

    class Meta:
        verbose_name_plural = "Catégories de couleurs des glandes buccales"


class BiomMamelle(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    short_descr = models.CharField(max_length=50, verbose_name=_('Mamelles'))
    descr = models.CharField(max_length=250, verbose_name=_('Mamelles'))

    def __str__(self):
        return "%s ∙ %s ∙ %s" % (self.id, self.code, self.short_descr)

    class Meta:
        verbose_name_plural = "Catégories d'état des mamelles"


class BiomGestation(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    short_descr = models.CharField(max_length=50, verbose_name=_('Gestation'))
    descr = models.CharField(max_length=100, verbose_name=_('Gestation'))

    def __str__(self):
        return "%s ∙ %s ∙ %s" % (self.id, self.code, self.short_descr)

    class Meta:
        verbose_name_plural = "Catégories d'état de la gestation"


class BiomEpiphyse(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    short_descr = models.CharField(max_length=50, verbose_name=_('Epiphyse'))
    descr = models.CharField(max_length=100, verbose_name=_('Epididyme'))

    def __str__(self):
        return "%s ∙ %s ∙ %s" % (self.id, self.code, self.short_descr)

    class Meta:
        verbose_name_plural = "Catégories d'état des épiphyses articulaires"


class BiomChinspot(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    short_descr = models.CharField(max_length=50, verbose_name=_('Tâche mentonière'))
    descr = models.CharField(max_length=100, verbose_name=_('Tâche mentonière'))

    def __str__(self):
        return "%s ∙ %s ∙ %s" % (self.id, self.code, self.short_descr)

    class Meta:
        verbose_name_plural = "Catégories d'état du chinspot"


class BiomDent(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=6)
    short_descr = models.CharField(max_length=50, verbose_name=_('Usure des dents'))
    descr = models.CharField(max_length=100, verbose_name=_('Usure des dents'))

    def __str__(self):
        return "%s ∙ %s ∙ %s" % (self.id, self.code, self.short_descr)

    class Meta:
        verbose_name_plural = _("Catégories d'état de l'usure des dents")


class PlaceManagementAction(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=5, verbose_name=_('Code des actions de gestion'))
    category = models.CharField(max_length=100, verbose_name=_("Catégorie d'action de gestion"))
    label = models.CharField(max_length=250, verbose_name=_("Action de gestion"))

    def __str__(self):
        return self.label

    class Meta:
        verbose_name = _("Dictionnaire des actions de gestion")


class LandCoverCLC(models.Model):
    code_lev3 = models.CharField(max_length=3, primary_key=True, verbose_name=_("Code Corine Land Cover (niv3)"))
    label_lev3 = models.CharField(max_length=150, verbose_name=_("Libelle Corine Land Cover (niv3)"))
    code_lev2 = models.CharField(max_length=3, verbose_name=_("Code Corine Land Cover (niv2)"))
    label_lev2 = models.CharField(max_length=150, verbose_name=_("Libelle Corine Land Cover (niv2)"))
    code_lev1 = models.CharField(max_length=3, verbose_name=_("Code Corine Land Cover (niv1)"))
    label_lev1 = models.CharField(max_length=150, verbose_name=_("Libelle Corine Land Cover (niv1)"))

    def __str__(self):
        return "%s ∙ %s" % (self.code_lev3, self.label_lev3)

    class Meta:
        verbose_name = _("Correspondance des codes CorineLandCover")