from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404, render
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from .mixins import BlogCreateAuthMixin, BlogModifyAuthMixin
from .forms import ActuForm
from .models import Actu
from sights.models import Place, Session, Sighting
from accounts.models import Profile


# Create your views here.

# class Actus(ListView):
#     model = Actu
#

class ActuCreate(BlogCreateAuthMixin, CreateView):
    model = Actu
    form_class = ActuForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.author = self.request.user
        return super(ActuCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ActuCreate, self).get_context_data(**kwargs)
        context['icon'] = 'fi-align-left'
        context['title'] = _('Ajout d\'une actualité')
        context['js'] = """      
        """
        return context


class ActuUpdate(BlogModifyAuthMixin, UpdateView):
    model = Actu
    form_class = ActuForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(ActuUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ActuUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'fi-align-left'
        context['title'] = _('Modification d\'une actualité')
        context['js'] = """      
        """
        return context


class ActuDelete(BlogModifyAuthMixin, DeleteView):
    model = Actu
    template_name = 'confirm_delete.html'
    success_url = reverse_lazy('blog:home')

    def get_context_data(self, **kwargs):
        context = super(ActuDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fi-trash'
        context['title'] = _('Suppression d\'une actualité')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer l\'actualité')
        return context


class ActuDetail(DetailView):
    model = Actu
    template_name = 'actu_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ActuDetail, self).get_context_data(**kwargs)
        context['icon'] = 'fi-align-left'
        context['js'] = """      
        """
        return context

class ActuList(ListView):
    model = Actu
    template_name = 'actu_list.html'
    ordering = '-timestamp_update'
    paginate_by = 3

    def get_context_data(self, **kwargs):
        context = super(ActuList, self).get_context_data(**kwargs)
        context['icon'] = 'fi-info'
        context['title'] = _('Dernières actualités')
        context['sightingsicon'] = 'fi-eye'
        context['sightingscount'] = Sighting.objects.all().count()
        context['sightingstext'] = _('observations')
        context['placesicon'] = 'fi-marker'
        context['placescount'] = Place.objects.all().count()
        context['placestext'] = _('localités')
        context['sessionsicon'] = 'fi-calendar'
        context['sessionscount'] = Session.objects.all().count()
        context['sessionstext'] = _('sessions')
        context['observersicon'] = 'fi-torsos-all-female'
        context['observerscount'] = Profile.objects.all().count()
        context['observerstext'] = _('observateurs')
        return context