from django.contrib.gis import forms
from crispy_forms.helper import FormHelper
from crispy_forms_foundation.layout import (AccordionHolder, AccordionItem, Fieldset, ButtonHolder, Column,
                                            Layout, Row, TabHolder, TabItem, Submit, Callout, HTML, InlineSwitchField)

from .models import Actu


class ActuForm(forms.ModelForm):
    class Meta:
        model = Actu
        fields = ('title', 'briefdescr', 'text')

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.form_show_errors = True
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            Row(
                Column('title'),
                Column('briefdescr'),
                Column('text'),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )

        super(ActuForm, self).__init__(*args, **kwargs)
