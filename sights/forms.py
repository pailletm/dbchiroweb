from crispy_forms.helper import FormHelper
from crispy_forms_foundation.layout import (AccordionHolder, AccordionItem, Fieldset, ButtonHolder, Column,
                                            Layout, Row, TabHolder, TabItem, Submit, Callout, InlineSwitchField)
from dal import autocomplete
from django.contrib.auth import get_user_model
from django.contrib.gis import forms
from django.core.exceptions import NON_FIELD_ERRORS
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from leaflet.forms.widgets import LeafletWidget

from dicts.models import TypeDevice, Method, CountPrecision, CountUnit
from management.models import Transmitter
from .models import (CountDetail, Place, Bridge, Build, PlaceManagement,
                     Cave, Tree, TreeGite, Session, Device, Sighting)


class PlaceForm(forms.ModelForm):
    class Meta:
        model = Place
        fields = (
            'name', 'is_hidden', 'authorized_user', 'precision', 'altitude', 'type', 'domain', 'geom', 'proprietary',
            'is_gite', 'x', 'y',
            'convention', 'map_file', 'is_managed', 'convention_file', 'photo_file', 'bdsource', 'id_bdsource',
            'comment',)
        widgets = {'geom': LeafletWidget(),
                   'authorized_user': autocomplete.ModelSelect2Multiple(url='api:all_user_autocomplete'), }
        readonly_fields = ('id_place')

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.form_show_errors = True
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem('Informations principales',
                              Row(
                                  Column(
                                      Callout(
                                          Fieldset(_("Caractéristiques principales"),
                                                   Column(
                                                       'name', css_class='small-12 large-12'),
                                                   Column('precision',
                                                          css_class='medium-4 small-12'),
                                                   Column('altitude',
                                                          css_class='medium-4 small-12'),
                                                   Column('domain',
                                                          css_class='medium-4 small-12'),
                                                   ),
                                          Fieldset(_("Sensibilité du site"),
                                                   Column(
                                                       InlineSwitchField('is_hidden', label_column='large-12',
                                                                         input_column='large-12'),
                                                       css_class='small-12 medium-3 large-2'
                                                   ),
                                                   Column(
                                                       'authorized_user', css_class='small-12 medium-9 large-10'), ),
                                          Fieldset("Gite et gestion",
                                                   Column(
                                                       InlineSwitchField('is_gite', label_column='large-12',
                                                                         input_column='large-12'),
                                                       css_class='small-12 large-4'
                                                   ),
                                                   Column('type', css_class='small-12 large-4'),
                                                   Column(
                                                       InlineSwitchField('is_managed', label_column='large-12',
                                                                         input_column='large-12'),
                                                       css_class='small-12 large-4'
                                                   ),
                                                   ),
                                          Fieldset("Localisation",
                                                   TabHolder(
                                                       TabItem('Par localisation sur carte',
                                                               Row(
                                                                   Column('geom', css_class='large-12'),
                                                               ),
                                                               ),
                                                       TabItem('Par coordonnées XY',
                                                               Row(
                                                                   Column('x', css_class='large-6'),
                                                                   Column('y', css_class='large-6'),
                                                               ),
                                                               ),
                                                   ),
                                                   ), css_class=''
                                      ), ), ), ),
                AccordionItem(
                    'Propriété, conventions et topo',
                    Row(
                        Column('proprietary', css_class='small-12 medium-12 large-4'),
                        Column(
                            InlineSwitchField('convention', label_column='large-12', input_column='large-12'),
                            css_class='small-12 medium-6 large-4'
                        ),
                        Column('convention_file',
                               css_class='small-12 medium-6 large-4'),
                    ), ),
                AccordionItem(
                    'Photo',
                    Row(
                        Column('photo_file', css_class='large-12'),
                    ),
                ),
                AccordionItem(
                    'Plan / Topo',
                    Row(
                        Column('map_file', css_class='large-12'),
                    ),
                ),
                AccordionItem(
                    'Source',
                    Row(
                        Column('bdsource', css_class='medium-6 small-12',
                               readonly=True),
                        Column('id_bdsource', readonly=True,
                               css_class='medium-6 small-12')
                    ),
                ),
                AccordionItem(
                    'Commentaire',
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )

        super(PlaceForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(PlaceForm, self).clean()
        is_gite = cleaned_data.get("is_gite")
        type = cleaned_data.get("type")

        if is_gite and not type:
            msg = _('Veuillez préciser le type de gîte')
            self.add_error('type', msg)


class PlaceSearchFilterForm(forms.Form):
    class Meta:
        widgets = {'territory': autocomplete.ListSelect2(url='api:territory_autocomplete'),
                   'municipality': autocomplete.ListSelect2(url='api:municipality_autocomplete')}

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'get'

        self.helper.layout = Layout(
            Row(
                Column('name', css_class='large-4'),
                Column('municipality', css_class='large-4'),
                Column('territory', css_class='large-4'),
                Column('type', css_class='large-4'),
            )
        )

        super(PlaceSearchFilterForm, self).__init__(*args, **kwargs)


class PlaceManagementForm(forms.ModelForm):
    class Meta:
        model = PlaceManagement
        fields = ('date', 'action', 'referent', 'comment', 'file')
        widgets = {'referent': autocomplete.ModelSelect2(url='api:all_user_autocomplete'), }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        # ======================================================================
        # self.fields['transmitter'].queryset = Transmitter.objects.filter(
        #     available is True)
        # ======================================================================
        self.helper.layout = Layout(
            AccordionHolder(
                AccordionItem(
                    _('Caractéristiques de l\'action'),
                    Row(
                        Column('date', css_class='large-4 small-12'),
                        Column('action', css_class='large-4 small-12'),
                        Column('referent', css_class='large-4 small-12')
                    ),
                ),
                AccordionItem(
                    _('Fichier'),
                    Row(
                        Column('file', css_class='large-12'),
                    ),
                ),
                AccordionItem(
                    _('commentaire'),
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )

        super(PlaceManagementForm, self).__init__(*args, **kwargs)


class BuildForm(forms.ModelForm):
    class Meta:
        model = Build
        fields = (
            'visit_date', 'cavity_front', 'attic', 'attic_access',
            'bell_tower', 'bell_tower_screen', 'bell_tower_access',
            'cover', 'ext_light', 'access_light', 'cellar', 'cellar_access', 'comment')

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem('Informations principales',
                              Row(
                                  Column(
                                      'visit_date', css_class='large-12'), ),
                              Callout(
                                  Fieldset(_('Combles'),
                                           Column(
                                               'attic', css_class='large-6'),
                                           Column(
                                               'attic_access', css_class='large-6'), ),
                                  Fieldset(_('Clocher'),
                                           Column(
                                               'bell_tower', css_class='large-4 small-6'),
                                           Column(
                                               'bell_tower_screen', css_class='large-4 small-6'),
                                           Column(
                                               'bell_tower_access', css_class='large-4 small-12'), ),
                                  Fieldset(_('Caves'),
                                           Column('cellar',
                                                  css_class='large-6'),
                                           Column('cellar_access',
                                                  css_class='large-6'), ),
                                  Fieldset(_('Autres éléments'),
                                           Column(
                                               'cavity_front', css_class='medium-3 small-6'),
                                           Column(
                                               'cover', css_class='medium-3 small-6'),
                                           Column(
                                               'ext_light', css_class='medium-3 small-6'),
                                           Column(
                                               'access_light', css_class='medium-3 small-6'), ), css_class='secondary'),
                              ),
                AccordionItem(
                    'Commentaire',
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button large'), css_class='text-right'
            ),
        )

        super(BuildForm, self).__init__(*args, **kwargs)


class CaveForm(forms.ModelForm):
    class Meta:
        model = Cave
        fields = ('visit_date', 'interest', 'length', 'altdiff',
                  'n_entry', 'equipment', 'access_walk_duration', 'comment',)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem(_('Informations principales'),
                              Row(
                                  Column(
                                      'visit_date', css_class='medium-6 small-6'),
                                  Column(
                                      'interest', css_class='medium-6 small-6'),
                              ),
                              Callout(
                                  Row(
                                      Column(
                                          'length', css_class='large-3 small-6'),
                                      Column(
                                          'altdiff', css_class='large-3 small-6'),
                                      Column(
                                          'n_entry', css_class='large-3 small-6'),
                                      Column(
                                          'access_walk_duration', css_class='large-3 small-6'),
                                  ), css_class="secondary"),
                              Row(

                                  Column(
                                      'equipment', css_class='large-12'),
                              ),
                              ),
                AccordionItem(
                    _('Commentaire'),
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )

        super(CaveForm, self).__init__(*args, **kwargs)


class BridgeForm(forms.ModelForm):
    class Meta:
        model = Bridge
        fields = ('visit_date', 'interest', 'renovated', 'renovated_date',
                  'joint', 'rift', 'expansion', 'drain', 'cornice', 'volume', 'comment',)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem(_('Informations principales'),
                              Row(
                                  Column(
                                      'visit_date', css_class='medium-6'),
                                  Column(
                                      'interest', css_class='medium-6'), ),
                              Fieldset('Historique de l\'ouvrage',
                                       Row(
                                           Column(
                                               'renovated', css_class='large-6 medium-4 small-12'),
                                           Column(
                                               'renovated_date', css_class='large-6 medium-4 small-12'),
                                       ), ),
                              Fieldset('Types de gîtes',
                                       Row(
                                           Column(
                                               'joint', css_class='small-12 medium-4'),
                                           Column(
                                               'rift', css_class='small-12 medium-4'),
                                           Column(
                                               'expansion', css_class='small-12 medium-4'),
                                           Column(
                                               'drain', css_class='small-12 medium-4'),
                                           Column(
                                               'cornice', css_class='small-12 medium-4'),
                                           Column(
                                               'volume', css_class='small-12 medium-4'), ),
                                       ), ),
                AccordionItem(
                    _('Commentaire'),
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )

        super(BridgeForm, self).__init__(*args, **kwargs)


class TreeForm(forms.ModelForm):
    class Meta:
        model = Tree
        fields = (
            'visit_date', 'context', 'forest_stands', 'situation', 'circumstance', 'tree_specie', 'health',
            'tree_diameter', 'standing', 'protected', 'bat_specie', 'gite_type', 'gite_origin',
            'gite_localisation', 'comment')
        widgets = {'visit_date': forms.DateInput,
                   'gite_type': forms.CheckboxSelectMultiple,
                   'gite_origin': forms.CheckboxSelectMultiple,
                   'gite_localisation': forms.CheckboxSelectMultiple,
                   'bat_specie': autocomplete.ModelSelect2Multiple(url='api:taxa_autocomplete'), }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem('Informations principales',
                              Row(
                                  Column(
                                      'visit_date', css_class='large-12'), ),
                              Callout(
                                  Fieldset(_('Contexte'),
                                           Row(
                                               Column(
                                                   'context', css_class='large-6 small-12'),
                                               Column(
                                                   'forest_stands', css_class='large-6 small-12'),
                                               Column(
                                                   'circumstance', css_class='large-6 small-12'),
                                               Column(
                                                   'situation', css_class='large-6 small-12'),
                                           ), ),
                                  Fieldset(_('Caractéristiques de l\'arbre'),
                                           Row(Column(
                                               'tree_specie', css_class='large-4 medium-12'),
                                               Column(
                                                   'health', css_class='large-4 medium-6'),
                                               Column(
                                                   'tree_diameter', css_class='large-4 medium-6'),
                                               Column(
                                                   'standing', css_class='large-6'),
                                               Column(
                                                   'protected', css_class='large-6'),
                                           ), ),
                                  Fieldset(_('Occupation'),
                                           Row(
                                               Column('bat_specie', css_class='large-12'),
                                           ), ),
                                  Fieldset(_('Caractérisation des gîtes'),
                                           Row(Column(
                                               Callout(
                                                   'gite_type'),
                                               css_class='large-6 small-12'),
                                               Column(
                                                   Callout(
                                                       'gite_origin'),
                                                   css_class='large-6 small-12'),
                                               Column(
                                                   Callout(
                                                       'gite_localisation'),
                                                   css_class='large-6 small-12'), ),
                                           ), css_class='secondary'
                              ), ),
                AccordionItem(
                    _('Commentaire'),
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )

        super(TreeForm, self).__init__(*args, **kwargs)


class TreeGiteForm(forms.ModelForm):
    class Meta:
        model = TreeGite
        fields = ('bat_specie', 'gite_type', 'gite_origin', 'gite_localisation', 'gite_high', 'gite_tree_diameter',
                  'gite_access_orientation', 'gite_access_size', 'comment',)
        widgets = {'bat_specie': autocomplete.ModelSelect2Multiple(url='api:taxa_autocomplete'), }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem(_('Informations principales'),
                              Callout(
                                  Fieldset(_('Occupation'),
                                           Column('bat_specie', css_class='large-12'), ),
                                  Fieldset(_('Caractéristiques du gîte'),
                                           Column(
                                               'gite_type', css_class='large-3 medium-3 small-6'),
                                           Column(
                                               'gite_origin', css_class='large-3 medium-3 small-6'),
                                           Column(
                                               'gite_localisation', css_class='large-3 medium-3 small-6'),
                                           Column(
                                               'gite_high', css_class='large-3 medium-3 small-6'),
                                           Column(
                                               'gite_tree_diameter', css_class='large-3 medium-3 small-6'),
                                           Column(
                                               'gite_access_orientation', css_class='large-3 medium-3 small-6'),
                                           Column(
                                               'gite_access_size', css_class='large-3 medium-3 small-6'),
                                           ), css_class='secondary'),
                              ),
                AccordionItem(
                    _('Commentaire'),
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),

        )

        super(TreeGiteForm, self).__init__(*args, **kwargs)


class SessionForm(forms.ModelForm):
    class Meta:
        model = Session
        fields = ('id_session', 'contact', 'date_start', 'time_start',
                  'date_end', 'data_file', 'study', 'is_confidential', 'time_end', 'main_observer', 'other_observer',
                  'comment')
        widgets = {'id_session': forms.HiddenInput(
        ), 'date_end': forms.DateInput(), 'date_end': forms.DateInput(),
            'main_observer': autocomplete.ModelSelect2(url='api:all_user_autocomplete'),
            'other_observer': autocomplete.ModelSelect2Multiple(url='api:all_user_autocomplete'), }
        error_messages = {
            NON_FIELD_ERRORS: {
                'unique_together': "%(model_name)s's %(field_labels)s are not unique.",
            }
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem('Informations principales',
                              Row(
                                  Column('contact',
                                         css_class='large-6 small-12'),
                                  Column('main_observer',
                                         css_class='large-6 small-12'),
                                  Column(
                                      'other_observer', css_class='large-12'),
                                  Column(
                                      'date_start', css_class='large-3 small-6'),
                                  Column(
                                      'time_start', css_class='large-3 small-6'),
                                  Column(
                                      'date_end', css_class='large-3 small-6'),
                                  Column(
                                      'time_end', css_class='large-3 small-6'),
                                  Column(
                                      InlineSwitchField('is_confidential', label_column='large-12',
                                                        input_column='large-12'),
                                      css_class='large-3 small-6'
                                  ),
                                  Column(
                                      'study', css_class='large-9 small-6'),
                              ), ),
                AccordionItem(
                    'Charger un fichier',
                    Row(
                        Column('data_file', css_class='large-12'),
                    ),
                ),
                AccordionItem(
                    'Commentaire',
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )

        super(SessionForm, self).__init__(*args, **kwargs)


class DeviceForm(forms.ModelForm):
    class Meta:
        model = Device
        fields = ('ref', 'type', 'height', 'width', 'context', 'photo_file', 'comment')

    def __init__(self, *args, **kwargs):
        contact = kwargs.pop('contact', None)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem(
                    'Dispositif',
                    Row(
                        Column('ref', css_class='large-12'),
                        Column('type', css_class='large-4 small-6'),
                        Column('height', css_class='large-4 small-6'),
                        Column('width', css_class='large-4 small-6'),
                        Column('context', css_class='large-12 small-12'),
                    ),
                ),
                AccordionItem(
                    'Photo du dispositif',
                    Row(
                        Column('photo_file', css_class='large-12'),
                    ),
                ),
                AccordionItem(
                    'Commentaire',
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),

            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )

        super(DeviceForm, self).__init__(*args, **kwargs)
        self.fields['type'].queryset = TypeDevice.objects.filter(contact=contact)


class SightingForm(forms.ModelForm):
    class Meta:
        model = Sighting
        fields = ('codesp', 'total_count', 'breed_colo', 'observer', 'is_doubtful',
                  'bdsource', 'id_bdsource', 'comment')
        widgets = {'geom': LeafletWidget(),
                   'codesp': autocomplete.ModelSelect2(url='api:taxa_autocomplete'), }

    def __init__(self, *args, **kwargs):
        main_observer = kwargs.pop('main_observer', None)
        other_observer = kwargs.pop('other_observer', None)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem('Espèces',
                              Row(
                                  Column('codesp',
                                         css_class='large-6 small-12'),
                                  Column('total_count',
                                         css_class='large-6 small-12'),
                              ),
                              Row(
                                  Column('observer',
                                         css_class='medium-12'),
                              ),
                              Row(
                                  Column('breed_colo', css_class='medium-6 small-6'),
                                  Column(
                                      InlineSwitchField('is_doubtful', label_column='large-12',
                                                        input_column='large-12'),
                                      css_class='large-6'
                                  ),
                              ),
                              ),
                AccordionItem(
                    'Autres informations',
                    Row(
                        Column('bdsource', css_class='medium-6 small-12',
                               readonly=True),
                        Column('id_bdsource', readonly=True,
                               css_class='large-6 small-12'),
                    ),
                ),
                AccordionItem(
                    'Commentaire',
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )

        super(SightingForm, self).__init__(*args, **kwargs)
        self.fields['observer'].queryset = get_user_model().objects.filter(
            Q(id__in=other_observer.all().values_list('id', flat=True)) | Q(id=main_observer.id)).distinct()


class CountDetailBiomForm(forms.ModelForm):
    class Meta:
        model = CountDetail
        fields = ('sex', 'age', 'time', 'device', 'method', 'manipulator', 'validator', 'transmitter', 'ab', 'd5',
                  'd3', 'pouce', 'queue', 'tibia', 'pied', 'cm3', 'tragus', 'poids', 'testicule',
                  'epididyme', 'tuniq_vag', 'gland_taille', 'gland_coul', 'mamelle', 'gestation',
                  'epiphyse', 'chinspot', 'usure_dent', 'comment')

    def __init__(self, *args, **kwargs):
        contact = kwargs.pop('contact', None)
        id_session = kwargs.pop('session', None)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem('Critères visuels et mesures',
                              Row(
                                  Column(
                                      Callout(
                                          Fieldset('Elements relatifs à l\'observation',
                                                   Column(
                                                       'manipulator', css_class='large-6 small-6'),
                                                   Column(
                                                       'validator', css_class='large-6 small-6'),
                                                   Column(
                                                       'time', css_class='large-4 small-6'),
                                                   Column(
                                                       'device', css_class='large-4 small-6'),
                                                   Column(
                                                       'method', css_class='large-4 small-6 end'),
                                                   ),
                                          Fieldset('Elements relevés',
                                                   Column(
                                                       'sex', css_class='large-6'),
                                                   Column(
                                                       'age', css_class='large-6'), ),
                                          Fieldset(_('Mesures biométriques'),
                                                   Column(
                                                       'ab', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'd5', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'd3', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'pouce', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'queue', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'tibia', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'pied', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'cm3', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'tragus', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'poids', css_class='large-2 medium-4 small-6 end'), ),
                                          Fieldset(_('Relevés visuels'),
                                                   Column(
                                                       'testicule', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'epididyme', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'tuniq_vag', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'gland_taille', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'gland_coul', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'mamelle', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'gestation', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'epiphyse', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'chinspot', css_class='large-2 medium-4 small-6'),
                                                   Column(
                                                       'usure_dent', css_class='large-2 medium-4 small-6 end'),
                                                   ), css_class='secondary'),
                                  ),
                              ),
                              ),
                AccordionItem(
                    'Emetteur de télémétrie',
                    Row(
                        Column('transmitter', css_class='large-12'),
                    ),
                ),
                AccordionItem(
                    'Commentaire',
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
        )

        super(CountDetailBiomForm, self).__init__(*args, **kwargs)
        self.fields['method'].queryset = Method.objects.filter(contact=contact)
        session = Session.objects.get(id_session=id_session)
        self.fields['manipulator'].queryset = get_user_model().objects.filter(Q(id__in=session.other_observer.all()) | Q(id=session.main_observer_id)).distinct()
        self.fields['validator'].queryset = get_user_model().objects.filter(Q(id__in=session.other_observer.all()) | Q(id=session.main_observer_id)).distinct()
        self.fields['device'].queryset = Device.objects.filter(session=id_session)
        self.fields['transmitter'].queryset = Transmitter.objects.filter(available=True)


class CountDetailAcousticForm(forms.ModelForm):
    class Meta:
        model = CountDetail
        fields = ('time', 'method', 'count', 'unit', 'comment')

    def __init__(self, *args, **kwargs):
        contact = kwargs.pop('contact', None)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem('Elements relatifs à l\'Observation',
                              Row(
                                  Column(
                                      Callout(
                                          Fieldset('Heure et éléments biologiques',
                                                   Row(
                                                       Column(
                                                           'method', css_class='large-6 small-12'),
                                                       Column(
                                                           'time', css_class='large-6 small-12'),
                                                   ), ),
                                          Fieldset('Comptage',
                                                   Row(
                                                       Column(
                                                           'count', css_class='large-6 small-6'),
                                                       Column(
                                                           'unit', css_class='large-6 small-6'),
                                                   ), ),
                                          css_class='secondary'),
                                  ), ),
                              ),
                AccordionItem(
                    'Commentaire',
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),

            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'

            ),
        )

        super(CountDetailAcousticForm, self).__init__(*args, **kwargs)
        self.fields['method'].queryset = Method.objects.filter(contact=contact)
        self.fields['unit'].queryset = CountUnit.objects.filter(contact=contact)


class CountDetailOtherForm(forms.ModelForm):
    class Meta:
        model = CountDetail
        fields = ('time', 'method', 'sex', 'age', 'count', 'unit', 'precision', 'transmitter', 'comment')

    def __init__(self, *args, **kwargs):
        contact = kwargs.pop('contact', None)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = 'submit'
        self.helper.layout = Layout(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'
            ),
            AccordionHolder(
                AccordionItem('Elements relatifs à l\'Observation',
                              Row(
                                  Column(
                                      Callout(
                                          Fieldset('Heure et éléments biologiques',
                                                   Row(
                                                       Column(
                                                           'method', css_class='large-6 small-12'),
                                                       Column(
                                                           'time', css_class='large-6 small-12'),
                                                       Column(
                                                           'sex', css_class='large-6'),
                                                       Column(
                                                           'age', css_class='large-6'),
                                                   ), ),
                                          Fieldset('Comptage',
                                                   Row(
                                                       Column(
                                                           'count', css_class='large-4 small-12'),
                                                       Column(
                                                           'unit', css_class='large-4 small-6'),
                                                       Column(
                                                           'precision', css_class='large-4 small-6'),
                                                   ), ),
                                          css_class='secondary'),
                                  ), ),
                              ),
                AccordionItem(
                    'Emetteur de télémétrie',
                    Row(
                        Column('transmitter', css_class='large-12'),
                    ),
                ),
                AccordionItem(
                    'Commentaire',
                    Row(
                        Column('comment', css_class='large-12'),
                    ),
                ),

            ),
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='button'), css_class='text-right'

            ),
        )

        super(CountDetailOtherForm, self).__init__(*args, **kwargs)
        self.fields['transmitter'].queryset = Transmitter.objects.filter(available=True)
        self.fields['method'].queryset = Method.objects.filter(contact=contact)
        self.fields['unit'].queryset = CountUnit.objects.filter(contact=contact)
        self.fields['precision'].queryset = CountPrecision.objects.filter(contact=contact)