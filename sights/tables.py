import django_tables2 as tables
from django.utils.translation import ugettext_lazy as _
from django_tables2.utils import A

from sights.models import Place, PlaceManagement, Bridge, Build, Cave, Device, Session, Sighting, Tree, TreeGite, CountDetail


class PlaceTable(tables.Table):
    ACTIONS = '''
        <a data-open="PlaceTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
    		<div class="reveal" id="SessionTableHistoryModal{{ record.pk }}" data-reveal>
    		    <h1>Historique de la donnée</h1>
        			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div> 
        <a href="{% url 'sights:place_detail' record.pk %}" edit"><i class="fi-info"></i></a>
        <a href="{% url 'sights:place_update' record.pk %}" delete"><i class="fi-page-edit"></i></a>
        <a href="{% url 'sights:place_delete' record.pk %}" delete"><i class="fi-trash"></i></a>
        '''
    # id = tables.Column(accessor='id_place', verbose_name='id')
    nbsession = tables.Column(accessor='session_set.all.count',
                              verbose_name="Sessions", orderable=False)
    name = tables.LinkColumn('sights:place_detail', args=[
        A('id_place')], accessor='name')
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False)
    type = tables.Column(accessor='type.descr', verbose_name="Type de gîte")

    class Meta:
        model = Place
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = ('actions', 'name', 'is_hidden', 'municipality', 'type', 'altitude', 'created_by',
                  'nbsession')


class PlaceManagementTable(tables.Table):
    ACTIONS = '''
        <a data-open="PlaceManagementTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
    	<div class="reveal" id="PlaceManagementTableHistoryModal{{ record.pk }}" data-reveal>
    		    <h1>Historique de la donnée</h1>
    			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
    				{% if t.timestamp_update %}
    				et mise à jour le {{ record.timestamp_update }}
    				par {{ record.updated_by }}
    				{% endif %}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div> 
        <a href="{% url 'sights:management_update' record.pk %}" title="Modifier"><i class="fi-page-edit"></i></a>
        <a href="{% url 'sights:management_delete' record.pk %}" title="Supprimer"><i class="fi-trash"></i></a>
        '''
    COMMENT = '''
            {% if record.comment %}
            <a data-open="PlaceManagementCommentModal{{ record.pk }}" title="Commentaire"><i class="fi-comments"></i></a>
        		<div class="reveal" id="PlaceManagementCommentModal{{ record.pk }}" data-reveal>
    		    <h1>Commentaire</h1>
    			    <p>{{ record.comment|safe }}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>
            {% endif %}
        '''
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False)
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = PlaceManagement
        template = 'table_foundation.html'
        attrs = {'class': 'responsive'}
        fields = ('actions', 'date', 'action', 'referent', 'file', 'comment')


class BridgeTable(tables.Table):
    ACTIONS = '''
            <a data-open="BridgeTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
    		<div class="reveal" id="BridgeTableHistoryModal{{ record.pk }}" data-reveal>
    		    <h1>Historique de la donnée</h1>
        			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>    
            <a href="{% url 'sights:bridge_update' record.pk %}" title="Modifier"><i class="fi-page-edit"></i></a>
            <a href="{% url 'sights:bridge_delete' record.pk %}" title="Supprimer"><i class="fi-trash"></i></a>
            '''
    COMMENT = '''
        {% if record.comment %}
        <a data-open="BridgeTableCommentModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-comments"></i></a>
    		<div class="reveal" id="BridgeTableCommentModal{{ record.pk }}" data-reveal>
		    <h1>Commentaire</h1>
			    <p>{{ record.comment|safe }}
				</p>
				<button class="close-button" data-close aria-label="Close modal" type="button">
				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
				</button>
			</div>
        {% endif %}
    '''
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = Bridge
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = ('actions', 'visit_date', 'interest', 'renovated', 'renovated_date',
                  'joint', 'rift', 'expansion', 'drain', 'cornice', 'volume', 'comment')


class TreeTable(tables.Table):
    ACTIONS = '''
            <a data-open="PlaceTreeDetailTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
    		<div class="reveal" id="PlaceTreeDetailTableHistoryModal{{ record.pk }}" data-reveal>
    		    <h1>Historique de la donnée</h1>
        			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>    
            <a href="{% url 'sights:tree_detail' record.pk %}" edit"><i class="fi-info"></i></a>
            <a href="{% url 'sights:tree_update' record.pk %}" title="Modifier"><i class="fi-page-edit"></i></a>
            <a href="{% url 'sights:tree_delete' record.pk %}" title="Supprimer"><i class="fi-trash"></i></a>
            '''
    COMMENT = '''
        {% if record.comment %}
        <a data-open="PlaceTreeDetailTableCommentModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-comments"></i></a>
    		<div class="reveal" id="PlaceTreeDetailTableCommentModal{{ record.pk }}" data-reveal>
		    <h1>Commentaire</h1>
			    <p>{{ record.comment|safe }}
				</p>
				<button class="close-button" data-close aria-label="Close modal" type="button">
				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
				</button>
			</div>
        {% endif %}
    '''
    NBTAX = '''
        {% ifequal 0 record.bat_specie.all.count %}
            -
        {% else %}
            <a  data-toggle="TreeSpecieListToggle{{ record.pk }}">{{ record.bat_specie.all.count }}</a>
            <div class="dropdown-pane" id="TreeSpecieListToggle{{ record.pk }}" data-dropdown>
                {% for d in record.bat_specie.all|dictsort:"sys_order" %}
                    <b>> {{ d.codesp }}{% if d.sp_true %}*{% endif %}</b> ({{ d.common_name_fr }})<br/>
                {% endfor %}
            </div>
        {% endifequal %}
        '''
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})
    nbtax = tables.TemplateColumn(
        NBTAX, verbose_name="Nb de taxons")
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = Tree
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = (
            'actions', 'visit_date', 'context', 'forest_stands', 'situation', 'circumstance', 'tree_specie', 'health',
            'tree_diameter', 'standing', 'protected', 'nbtax', 'gite_type', 'gite_origin',
            'gite_localisation', 'comment')


class TreeGiteTable(tables.Table):
    ACTIONS = '''
            <a data-open="TreeGiteTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
    		<div class="reveal" id="TreeGiteTableHistoryModal{{ record.pk }}" data-reveal>
    		    <h1>Historique de la donnée</h1>
        			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>    
            <a href="{% url 'sights:treegite_update' record.pk %}" title="Modifier"><i class="fi-page-edit"></i></a>
            <a href="{% url 'sights:treegite_delete' record.pk %}" title="Supprimer"><i class="fi-trash"></i></a>
            '''
    COMMENT = '''
        {% if record.comment %}
        <a data-open="PlaceDetailSessionTableCommentModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-comments"></i></a>
    		<div class="reveal" id="PlaceDetailSessionTableCommentModal{{ record.pk }}" data-reveal>
		    <h1>Commentaire</h1>
			    <p>{{ record.comment|safe }}
				</p>
				<button class="close-button" data-close aria-label="Close modal" type="button">
				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
				</button>
			</div>
        {% endif %}
    '''
    NBTAX = '''
        {% ifequal 0 record.bat_specie.all.count %}
            -
        {% else %}
            <a  data-toggle="TreeSpecieListToggle{{ record.pk }}">{{ record.bat_specie.all.count }}</a>
            <div class="dropdown-pane" id="TreeSpecieListToggle{{ record.pk }}" data-dropdown>
                {% for d in record.bat_specie.all|dictsort:"sys_order" %}
                    <b>> {{ d.codesp }}{% if d.sp_true %}*{% endif %}</b> ({{ d.common_name_fr }})<br/>
                {% endfor %}
            </div>
        {% endifequal %}
        '''
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})
    nbtax = tables.TemplateColumn(
        NBTAX, verbose_name="Nb de taxons")
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = TreeGite
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = (
            'actions', 'nbtax', 'gite_type', 'gite_origin', 'gite_localisation', 'gite_high', 'gite_tree_diameter',
            'gite_access_orientation', 'gite_access_size', 'comment')


class BuildTable(tables.Table):
    ACTIONS = '''
                <a data-open="BuildDetailTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
        		<div class="reveal" id="BuildDetailTableHistoryModal{{ record.pk }}" data-reveal>
        		    <h1>Historique de la donnée</h1>
        			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
        				<button class="close-button" data-close aria-label="Close modal" type="button">
        				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
        				</button>
        			</div>    
                <a href="{% url 'sights:build_update' record.pk %}" title="Modifier"><i class="fi-page-edit"></i></a>
                <a href="{% url 'sights:build_delete' record.pk %}" title="Supprimer"><i class="fi-trash"></i></a>
                '''
    COMMENT = '''
            {% if record.comment %}
            <a data-open="BuildTableCommentModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-comments"></i></a>
        		<div class="reveal" id="BuildTableCommentModal{{ record.pk }}" data-reveal>
    		    <h1>Commentaire</h1>
    			    <p>{{ record.comment|safe }}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>
            {% endif %}
        '''
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = Build
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = ('actions', 'visit_date', 'cavity_front', 'attic', 'attic_access',
                  'bell_tower', 'bell_tower_screen', 'bell_tower_access',
                  'cover', 'ext_light', 'access_light', 'cellar', 'cellar_access', 'comment')


class CaveTable(tables.Table):
    ACTIONS = '''
                <a data-open="BuildDetailTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
        		<div class="reveal" id="BuildDetailTableHistoryModal{{ record.pk }}" data-reveal>
        		    <h1>Historique de la donnée</h1>
        			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
        				<button class="close-button" data-close aria-label="Close modal" type="button">
        				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
        				</button>
        			</div>    
                <a href="{% url 'sights:cave_update' record.pk %}" title="Modifier"><i class="fi-page-edit"></i></a>
                <a href="{% url 'sights:cave_delete' record.pk %}" title="Supprimer"><i class="fi-trash"></i></a>
                '''
    COMMENT = '''
            {% if record.comment %}
            <a data-open="BuildTableCommentModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-comments"></i></a>
        		<div class="reveal" id="BuildTableCommentModal{{ record.pk }}" data-reveal>
    		    <h1>Commentaire</h1>
    			    <p>{{ record.comment|safe }}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>
            {% endif %}
        '''
    WALK_DURATION = '''
            {{ record.access_walk_duration|time:"H:i" }}'''
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)
    access_walk_duration = tables.TemplateColumn(
        WALK_DURATION, verbose_name='Durée de marche', orderable=False)

    class Meta:
        model = Cave
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = ('actions', 'visit_date', 'interest', 'length', 'altdiff',
                  'n_entry', 'equipment', 'access_walk_duration', 'comment')


class SessionTable(tables.Table):
    ACTIONS = '''
            <a data-open="SessionTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
    		<div class="reveal" id="SessionTableHistoryModal{{ record.pk }}" data-reveal>
    		    <h1>Historique de la donnée</h1>
        			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>    
            <a href="{% url 'sights:session_detail' record.pk %}" title="Fiche détaillée"><i class="fi-info"></i></a>
            <a href="{% url 'sights:session_update' record.pk %}" title="Modifier"><i class="fi-page-edit"></i></a>
            <a href="{% url 'sights:sighting_create' record.pk %}" Title="Ajouter une observation"><i class="fi-eye"></i></a>
            <a href="{% url 'sights:session_delete' record.pk %}" title="Supprimer"><i class="fi-trash"></i></a>
            '''
    NBTAX = '''
            {% ifequal 0 record.sighting_set.all.count %}
            -
            {% else %}
                <a  data-toggle="TreeSpecieListToggle{{ record.pk }}">{{ record.sighting_set.all.count }}</a>
                <div class="dropdown-pane" id="TreeSpecieListToggle{{ record.pk }}" data-dropdown>
                    {% for d in record.sighting_set.all %}
                        <b>> {{ d.codesp.codesp }}{% if d.codesp.sp_true %}*{% endif %}</b> ({{ d.codesp.common_name_fr }})<br/>
                    {% endfor %}
                </div>
            {% endifequal %}
    '''
    OBSERVERS = '''
        {{ record.main_observer }}
        {% if record.other_observer.all.count > 0 %}
            <a data-toggle="SessionObserverListToggle{{ record.pk }}">{{ record.other_observer.all.count }}</a>
            <div class="dropdown-pane" id="SessionObserverListToggle{{ record.pk }}" data-dropdown>

                {% for o in record.other_observer.all %}
                    <b>{{ o.get_full_name }}</b> ({{ o.username}})<br/>
                {% endfor %}

            </div>
        {% endif %}
        '''
    COMMENT = '''
        {% if record.comment %}
        <a data-open="PlaceDetailSessionTableCommentModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-comments"></i></a>
    		<div class="reveal" id="PlaceDetailSessionTableCommentModal{{ record.pk }}" data-reveal>
		    <h1>Commentaire</h1>
			    <p>{{ record.comment|safe }}
				</p>
				<button class="close-button" data-close aria-label="Close modal" type="button">
				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
				</button>
			</div>
        {% endif %}
    '''
    # id = tables.Column(accessor='id_session', verbose_name='id')
    name = tables.Column(verbose_name='Nom')
    nbtax = tables.TemplateColumn(
        NBTAX, verbose_name="Nb de taxons")
    observer = tables.TemplateColumn(
        OBSERVERS, verbose_name="Observateur(s)")
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)
    date_start = tables.Column(verbose_name="Date début")
    date_end = tables.Column(verbose_name="Date fin")
    time_start = tables.Column(verbose_name="Heure début")
    time_end = tables.Column(verbose_name="Heure fin")
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})
    name = tables.LinkColumn('sights:session_detail', args=[
        A('id_session')], accessor='name')
    place = tables.LinkColumn('sights:place_detail', args=[
        A('place.id_place')], accessor='place')

    class Meta:
        model = Session
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = ('actions', 'name', 'place', 'place.municipality', 'contact.descr', 'date_start', 'time_start',
                  'date_end', 'time_end', 'observer', 'nbtax', 'comment')


class PlaceSessionTable(tables.Table):
    ACTIONS = '''
        <a data-open="PlaceDetailSessionTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
    		<div class="reveal" id="PlaceDetailSessionTableHistoryModal{{ record.pk }}" data-reveal>
		    <h1>Historique de la donnée</h1>
        			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
				<button class="close-button" data-close aria-label="Close modal" type="button">
				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
				</button>
			</div>    
        <a href="{% url 'sights:session_detail' record.pk %}" title="Fiche détaillée"><i class="fi-info"></i></a>
        <a href="{% url 'sights:session_update' record.pk %}" title="Modifier"><i class="fi-page-edit"></i></a>
        <a href="{% url 'sights:sighting_create' record.pk %}" Title="Ajouter une observation"><i class="fi-eye"></i></a>
        <a href="{% url 'sights:session_delete' record.pk %}" title="Supprimer"><i class="fi-trash"></i></a>
        '''
    NBTAX = '''
            {% ifequal 0 record.sighting_set.all.count %}
            -
            {% else %}
                <a  data-toggle="TreeSpecieListToggle{{ record.pk }}">{{ record.sighting_set.all.count }}</a>
                <div class="dropdown-pane" id="TreeSpecieListToggle{{ record.pk }}" data-dropdown>
                    {% for d in record.sighting_set.all %}
                        <b>> {{ d.codesp.codesp }}{% if d.codesp.sp_true %}*{% endif %}</b> ({{ d.codesp.common_name_fr }})<br/>
                    {% endfor %}
                </div>
            {% endifequal %}
        '''
    OBSERVERS = '''
        {{ record.main_observer }}
        {% if record.other_observer.all.count > 0 %}
            <a data-toggle="SessionObserverListToggle{{ record.pk }}">{{ record.other_observer.all.count }}</a>
            <div class="dropdown-pane" id="SessionObserverListToggle{{ record.pk }}" data-dropdown>

                {% for o in record.other_observer.all %}
                    <b>{{ o.username }}</b> ({{ o.first_name }} {{ o.last_name }})</br>
                {% endfor %}

            </div>
        {% endif %}
        '''
    COMMENT = '''
        {% if record.comment %}
        <a data-open="PlaceDetailSessionTableCommentModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-comments"></i></a>
    		<div class="reveal" id="PlaceDetailSessionTableCommentModal{{ record.pk }}" data-reveal>
		    <h1>Commentaire</h1>
			    <p>{{ record.comment|safe }}
				</p>
				<button class="close-button" data-close aria-label="Close modal" type="button">
				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
				</button>
			</div>
        {% endif %}
    '''
    # id = tables.Column(accessor='id_session', verbose_name='id')
    name = tables.LinkColumn('sights:session_detail', args=[
        A('id_session')], accessor='name', verbose_name='Nom')
    nbtax = tables.TemplateColumn(
        NBTAX, verbose_name="Nb de taxons", orderable=False)
    observer = tables.TemplateColumn(
        OBSERVERS, verbose_name="Observateur(s)")
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)
    date_start = tables.Column(verbose_name="Date début")
    date_end = tables.Column(verbose_name="Date fin")
    time_start = tables.Column(verbose_name="Heure début")
    time_end = tables.Column(verbose_name="Heure fin")
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})

    class Meta:
        model = Session
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = ('actions', 'name', 'contact.descr', 'date_start', 'time_start',
                  'date_end', 'time_end', 'observer', 'nbtax', 'comment')


class SessionSightingTable(tables.Table):
    ACTIONS = '''
        <a data-open="SightingTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
    		<div class="reveal" id="SightingTableHistoryModal{{ record.pk }}" data-reveal>
                <h1>Historique de la donnée</h1>
        			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
				<button class="close-button" data-close aria-label="Close modal" type="button">
				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
				</button>
			</div> 
        <a href="{% url 'sights:sighting_detail' record.pk %}" edit"><i class="fi-info"></i></a>
        <a href="{% url 'sights:sighting_update' record.pk %}" delete"><i class="fi-page-edit"></i></a>
        <a href="{% url 'sights:sighting_delete' record.pk %}" delete"><i class="fi-trash"></i></a>
        '''
    COMMENT = '''
            {% if record.comment %}
            <a data-open="PlaceDetailSessionTableCommentModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-comments"></i></a>
        		<div class="reveal" id="PlaceDetailSessionTableCommentModal{{ record.pk }}" data-reveal>
    		    <h1>Commentaire</h1>
    			    <p>{{ record.comment|safe }}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>
            {% endif %}
        '''
    # id = tables.Column(accessor='id_sighting', verbose_name='id')
    common_name_fr = tables.LinkColumn('sights:sighting_detail', args=[
        A('id_sighting')], accessor='codesp.common_name_fr', verbose_name='Taxon')
    # common_name_fr = tables.Column(
    #     accessor='codesp.common_name_fr', verbose_name='Taxon')
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)
    sp_true = tables.BooleanColumn(verbose_name=_('Espèce vraie'))
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})

    class Meta:
        model = Sighting
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = ('actions', 'common_name_fr', 'sp_true', 'total_count', 'breed_colo', 'created_by', 'comment')


class SessionDeviceTable(tables.Table):
    ACTIONS = '''
        <a data-open="SessionDeviceTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
    		<div class="reveal" id="SessionDeviceTableHistoryModal{{ record.pk }}" data-reveal>
                <h1>Historique de la donnée</h1>
        			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
				<button class="close-button" data-close aria-label="Close modal" type="button">
				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
				</button>
			</div> 
        <a href="{% url 'sights:device_update' record.pk %}" delete"><i class="fi-page-edit"></i></a>
        <a href="{% url 'sights:device_delete' record.pk %}" delete"><i class="fi-trash"></i></a>
        '''
    COMMENT = '''
            {% if record.comment %}
            <a data-open="SessionDeviceTableCommentModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-comments"></i></a>
        		<div class="reveal" id="SessionDeviceTableCommentModal{{ record.pk }}" data-reveal>
    		    <h1>Commentaire</h1>
    			    <p>{{ record.comment|safe }}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>
            {% endif %}
        '''
    PHOTO_FILE = '''
            {% if record.photo_file %}
            <a data-open="SessionDeviceTablePhotoModal{{ record.pk }}" title="Photo du dispositif"><i class="fi-camera"></i></a>
        		<div class="reveal" id="SessionDeviceTablePhotoModal{{ record.pk }}" data-reveal>
    		    <h1>Photo du dispositif</h1>
    			    <p><img src="{{ placedetail.photo_file.url }}" width="100%"></p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>
            {% endif %}
    
    '''
    # id = tables.Column(accessor='id_device', verbose_name='id')
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)
    photo = tables.TemplateColumn(
        PHOTO_FILE, verbose_name="Photo", orderable=False)
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})

    class Meta:
        model = Device
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = ('actions', 'ref', 'type', 'height', 'width', 'photo', 'comment')


class SightingTable(tables.Table):
    ACTIONS = '''
        <a data-open="SightingTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
    		<div class="reveal" id="SightingTableHistoryModal{{ record.pk }}" data-reveal>
                <h1>Historique de la donnée</h1>
        			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
				<button class="close-button" data-close aria-label="Close modal" type="button">
				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
				</button>
			</div> 
        <a href="{% url 'sights:sighting_detail' record.pk %}" edit"><i class="fi-info"></i></a>
        <a href="{% url 'sights:sighting_update' record.pk %}" delete"><i class="fi-page-edit"></i></a>
        <a href="{% url 'sights:sighting_delete' record.pk %}" delete"><i class="fi-trash"></i></a>
        '''
    COMMENT = '''
            {% if record.comment %}
            <a data-open="PlaceDetailSessionTableCommentModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-comments"></i></a>
        		<div class="reveal" id="PlaceDetailSessionTableCommentModal{{ record.pk }}" data-reveal>
    		    <h1>Commentaire</h1>
    			    <p>{{ record.comment|safe }}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>
            {% endif %}
        '''
    PLACETYPE = '''
        {% if record.session.place.type %}
        <a data-toggle="SightingSessionPlaceTypeToggle{{ record.pk }}">{{ record.session.place.type.code }}</a>
            <div class="dropdown-pane" id="SightingSessionPlaceTypeToggle{{ record.pk }}" data-dropdown>
                {{ record.session.place.type.descr }}
            </div>
        {% endif %}
    '''
    METHOD = '''
        {% if record.session.contact %}
        <a data-toggle="SightingSessionMethodToggle{{ record.pk }}">{{ record.session.contact.code }}</a>
            <div class="dropdown-pane" id="SightingSessionMethodToggle{{ record.pk }}" data-dropdown>
                {{ record.record.session.contact.descr }}
            </div>
        {% endif %}
    '''
    SESSION = '''
    <a data-toggle="SightingSessionToggle{{ record.pk }}">{{ record.session.name }}</a>
            <div class="dropdown-pane" id="SightingSessionToggle{{ record.pk }}" data-dropdown>
                Créateur : {{ record.session.created_by }}</br>
                Début : {{ record.session.date_start }}</br>
                Méthode: {{ record.session.contact.descr }}</br>
                <a class="button" href="{% url 'sights:session_detail' record.session.id_session %}">Aller à la fiche de la session</a>
            </div>
    
    '''
    common_name_fr = tables.LinkColumn('sights:sighting_detail', args=[A('id_sighting')],
                                       accessor='codesp.common_name_fr', verbose_name='Espèce')
    sp_true = tables.BooleanColumn(accessor='codesp.sp_true',
                            verbose_name='Espèce vraie')
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)
    place = tables.LinkColumn('sights:place_detail', args=[
        A('session.place.id_place')], accessor='session.place')
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})
    placetype = tables.TemplateColumn(
        PLACETYPE, accessor='session.place.type.descr', verbose_name='Type de localité')
    session = tables.TemplateColumn(
        SESSION, accessor='session.name', verbose_name='Session')

    class Meta:
        model = Sighting
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = ('actions', 'common_name_fr', 'sp_true', 'total_count', 'breed_colo', 'session', 'session.date_start',
                  'session.contact.descr', 'place', 'placetype', 'session.place.municipality', 'created_by', 'comment')


class SightingExportTable(tables.Table):
    id = tables.Column(accessor='id_sighting', verbose_name='id')
    common_name_fr = tables.Column(accessor='codesp.common_name_fr', verbose_name='Espèce')
    sp_true = tables.Column(accessor='codesp.sp_true',
                            verbose_name='Espèce vraie')
    x = tables.Column(accessor='session.place.geom.x', verbose_name='X')
    y = tables.Column(accessor='session.place.geom.y', verbose_name='Y')
    place = tables.Column(accessor='session.place', verbose_name='Localité')
    placetype = tables.Column(
        accessor='session.place.type.descr', verbose_name='Type de localité')

    class Meta:
        model = Sighting
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = ('actions', 'common_name_fr', 'sp_true', 'total_count', 'breed_colo', 'session',
                  'place', 'placetype', 'session.place.municipality',
                  'created_by', 'comment', 'geom', 'x', 'y')


class CountDetailBiomTable(tables.Table):
    ACTIONS = '''
            <a data-open="CountDetailTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
        		<div class="reveal" id="CountDetailTableHistoryModal{{ record.pk }}" data-reveal>
                    <h1>Historique de la donnée</h1>
         			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div> 
            <a href="{% url 'sights:countdetail_update' record.pk %}" delete"><i class="fi-page-edit"></i></a>
            <a href="{% url 'sights:countdetail_delete' record.pk %}" delete"><i class="fi-trash"></i></a>
            '''
    COMMENT = '''
            {% if record.comment %}
            <a data-open="CountDetailTableCommentModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-comments"></i></a>
        		<div class="reveal" id="CountDetailTableCommentModal{{ record.pk }}" data-reveal>
    		    <h1>Commentaire</h1>
    			    <p>{{ record.comment|safe }}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>
            {% endif %}
            '''
    TIME = '''
            {{ record.time|time:"H:i" }}'''
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)
    time = tables.TemplateColumn(
        TIME, verbose_name=_('Heure'), orderable=False)

    class Meta:
        model = CountDetail
        template = 'table_foundation.html'
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = (
            'actions', 'method.descr', 'sex', 'age', 'time', 'device', 'manipulator', 'validator', 'transmitter', 'ab', 'd5',
            'd3', 'pouce', 'queue', 'tibia', 'pied', 'cm3', 'tragus', 'poids', 'testicule.short_descr',
            'epididyme.short_descr', 'tuniq_vag.short_descr', 'gland_taille.short_descr', 'gland_coul.short_descr',
            'mamelle.short_descr', 'gestation.short_descr', 'epiphyse.short_descr', 'chinspot.short_descr',
            'usure_dent.short_descr', 'comment')


class CountDetailOtherTable(tables.Table):
    ACTIONS = '''
            <a data-open="CountDetailTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
        		<div class="reveal" id="CountDetailTableHistoryModal{{ record.pk }}" data-reveal>
                    <h1>Historique de la donnée</h1>
         			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div> 
            <a href="{% url 'sights:countdetail_update' record.pk %}" delete"><i class="fi-page-edit"></i></a>
            <a href="{% url 'sights:countdetail_delete' record.pk %}" delete"><i class="fi-trash"></i></a>
            '''
    COMMENT = '''
            {% if record.comment %}
            <a data-open="CountDetailTableCommentModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-comments"></i></a>
        		<div class="reveal" id="CountDetailTableCommentModal{{ record.pk }}" data-reveal>
    		    <h1>Commentaire</h1>
    			    <p>{{ record.comment|safe }}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>
            {% endif %}
            '''
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = CountDetail
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = ('time', 'method', 'sex', 'age', 'count', 'unit', 'precision', 'transmitter', 'comment')


class CountDetailAcousticTable(tables.Table):
    ACTIONS = '''
            <a data-open="CountDetailTableHistoryModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-clock"></i></a>
        		<div class="reveal" id="CountDetailTableHistoryModal{{ record.pk }}" data-reveal>
                    <h1>Historique de la donnée</h1>
         			    <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
        				{% if record.timestamp_update %}
        				et mise à jour le {{ record.timestamp_update }}
        				{% endif %}
        				par {{ record.updated_by }}
        				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div> 
            <a href="{% url 'sights:countdetail_update' record.pk %}" delete"><i class="fi-page-edit"></i></a>
            <a href="{% url 'sights:countdetail_delete' record.pk %}" delete"><i class="fi-trash"></i></a>
            '''
    COMMENT = '''
            {% if record.comment %}
            <a data-open="CountDetailTableCommentModal{{ record.pk }}" title="Historique de la donnée"><i class="fi-comments"></i></a>
        		<div class="reveal" id="CountDetailTableCommentModal{{ record.pk }}" data-reveal>
    		    <h1>Commentaire</h1>
    			    <p>{{ record.comment|safe }}
    				</p>
    				<button class="close-button" data-close aria-label="Close modal" type="button">
    				<span aria-hidden="true"><i class="fi-x-circle"></i></span>
    				</button>
    			</div>
            {% endif %}
            '''
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = CountDetail
        template = 'table_foundation.html'
        attrs = {'class': 'responsive hover'}
        fields = ('actions', 'time', 'method', 'count', 'unit', 'precision', 'comment')
