import django_filters
from dal import autocomplete
from django.utils.translation import ugettext_lazy as _

from dicts.models import TypePlace
from sights.models import Place


class PlaceFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(label=_('Nom de la localité'), lookup_expr='icontains')
    municipality = django_filters.CharFilter(name="municipality__id", label=_('Commune'),
                                             widget=autocomplete.ListSelect2(url='api:municipality_autocomplete', attrs={'class':'listselect2'}))
    territory = django_filters.CharFilter(name="territory_id", label=_('Département'),
                                          widget=autocomplete.ListSelect2(url='api:territory_autocomplete', attrs={'class':'listselect2'}))
    type = django_filters.ModelChoiceFilter(name="type", label=_('Type de localité'), queryset=TypePlace.objects.all())

    class Meta:
        model = Place
        fields = [
            'name', 'municipality', 'territory', 'type'
        ]
        order_by = ['name']
