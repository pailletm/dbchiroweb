from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

from sights.views import (PlaceUpdate, PlaceDelete, PlaceList, PlaceDetail, PlaceManagementCreate,
                          PlaceManagementUpdate, PlaceManagementDelete, BuildCreate, BuildUpdate, BuildDelete,
                          BridgeCreate, BridgeUpdate, BridgeDelete,
                          CaveCreate, CaveUpdate, CaveDelete, TreeCreate, TreeUpdate, TreeDelete, TreeDetail,
                          TreeGiteCreate,
                          TreeGiteUpdate, TreeGiteDelete,
                          SessionCreate, SessionDetail, SessionUpdate, SessionDelete, SessionMyList, SightingCreate,
                          SightingUpdate, SightingDelete, SightingDetail, SightingList,
                          SightingMyList, DeviceCreate, DeviceUpdate, DeviceDelete,
                          CountDetailCreate, CountDetailUpdate, CountDetailDelete,
                          PlaceCreate, PlaceMyList, SightingUserList)

urlpatterns = [
    # url de recherches de localités
    url(r'^place/search$', PlaceList.as_view(), name='place_search'),

    # User views relative URLS
    url(r'^user/place/list$',
        PlaceMyList.as_view(), name='place_mylist'),
    url(r'^user/session/list$',
        SessionMyList.as_view(), name='session_mylist'),
    url(r'^user/sighting/list$',
        SightingMyList.as_view(), name='sighting_mylist'),
    url(r'^user/(?P<pk>[0-9]+)/sighting/list',
        SightingUserList.as_view(), name='user_sighting_list'),

    # Place views
    url(r'^place/map$', login_required(TemplateView.as_view(
        template_name='place_map.html')), name='place_map'),
    url(r'^place/tilemap$', login_required(TemplateView.as_view(
        template_name='place_tilemap.html')), name='place_tilemap', ),
    url(r'^place/add$', PlaceCreate.as_view(), name='place_create'),
    url(r'^place/(?P<pk>\d+)/detail$',
        PlaceDetail.as_view(), name='place_detail'),
    url(r'^place/(?P<pk>[0-9]+)/update$',
        PlaceUpdate.as_view(), name='place_update'),
    url(r'^place/(?P<pk>[0-9]+)/delete$',
        PlaceDelete.as_view(), name='place_delete'),

    # Place management relative URLS
    url(r'^place/(?P<pk>[0-9]+)/management/add$',
        PlaceManagementCreate.as_view(), name='management_create'),
    url(r'^management/(?P<pk>[0-9]+)/update$',
        PlaceManagementUpdate.as_view(), name='management_update'),
    url(r'^management/(?P<pk>[0-9]+)/delete$',
        PlaceManagementDelete.as_view(), name='management_delete'),

    # Build views
    url(r'^place/(?P<pk>[0-9]+)/build/add$',
        BuildCreate.as_view(), name='build_create'),
    url(r'^build/(?P<pk>[0-9]+)/update$',
        BuildUpdate.as_view(), name='build_update'),
    url(r'^build/(?P<pk>[0-9]+)/delete$',
        BuildDelete.as_view(), name='build_delete'),

    # Tree views
    url(r'^place/(?P<pk>[0-9]+)/tree/add$',
        TreeCreate.as_view(), name='tree_create'),
    url(r'^tree/(?P<pk>[0-9]+)/update$',
        TreeUpdate.as_view(), name='tree_update'),
    url(r'^tree/(?P<pk>[0-9]+)/delete$',
        TreeDelete.as_view(), name='tree_delete'),
    url(r'^tree/(?P<pk>[0-9]+)/detail$',
        TreeDetail.as_view(), name='tree_detail'),

    # TreeGite views
    url(r'^tree/(?P<pk>[0-9]+)/treegite/add$',
        TreeGiteCreate.as_view(), name='treegite_create'),
    url(r'^treegite/(?P<pk>[0-9]+)/update$',
        TreeGiteUpdate.as_view(), name='treegite_update'),
    url(r'^treegite/(?P<pk>[0-9]+)/delete$',
        TreeGiteDelete.as_view(), name='treegite_delete'),

    # Cave views
    url(r'^place/(?P<pk>[0-9]+)/cave/add$',
        CaveCreate.as_view(), name='cave_create'),
    url(r'^cave/(?P<pk>[0-9]+)/update$',
        CaveUpdate.as_view(), name='cave_update'),
    url(r'^cave/(?P<pk>[0-9]+)/delete$',
        CaveDelete.as_view(), name='cave_delete'),

    # Bridge views
    url(r'^place/(?P<pk>[0-9]+)/bridge/add$',
        BridgeCreate.as_view(), name='bridge_create'),
    url(r'^bridge/(?P<pk>[0-9]+)/update$',
        BridgeUpdate.as_view(), name='bridge_update'),
    url(r'^bridge/(?P<pk>[0-9]+)/delete$',
        BridgeDelete.as_view(), name='bridge_delete'),

    # Session relative URLS
    url(r'^place/(?P<pk>[0-9]+)/session/add$',
        SessionCreate.as_view(), name='session_create'),
    url(r'^session/(?P<pk>[0-9]+)/detail$',
        SessionDetail.as_view(), name='session_detail'),
    url(r'^session/(?P<pk>[0-9]+)/update$',
        SessionUpdate.as_view(), name='session_update'),
    url(r'^session/(?P<pk>[0-9]+)/delete$',
        SessionDelete.as_view(), name='session_delete'),

    # Device relative URLS
    url(r'^session/(?P<pk>[0-9]+)/device/add$',
        DeviceCreate.as_view(), name='device_create'),
    url(r'^device/(?P<pk>[0-9]+)/update$',
        DeviceUpdate.as_view(), name='device_update'),
    url(r'^device/(?P<pk>[0-9]+)/delete$',
        DeviceDelete.as_view(), name='device_delete'),

    # Sighting relative URLS
    url(r'^session/(?P<pk>[0-9]+)/sighting/add$',
        SightingCreate.as_view(), name='sighting_create'),
    url(r'^sighting/(?P<pk>[0-9]+)/detail$',
        SightingDetail.as_view(), name='sighting_detail'),
    url(r'^sighting/(?P<pk>[0-9]+)/update$',
        SightingUpdate.as_view(), name='sighting_update'),
    url(r'^sighting/(?P<pk>[0-9]+)/delete$',
        SightingDelete.as_view(), name='sighting_delete'),
    url(r'^sighting/list$',
        SightingList.as_view(), name='sighting_list'),

    # CountDetail relative URLS
    url(r'^sighting/(?P<pk>[0-9]+)/countdetail/add$',
        CountDetailCreate.as_view(), name='countdetail_create'),
    url(r'^countdetail/(?P<pk>[0-9]+)/update$',
        CountDetailUpdate.as_view(), name='countdetail_update'),
    url(r'^countdetail/(?P<pk>[0-9]+)/delete$',
        CountDetailDelete.as_view(), name='countdetail_delete'),
]
