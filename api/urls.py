# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from djgeojson.views import GeoJSONLayerView

from sights.models import Place, Territory, Municipality
from .views import TiledGeoJSONData, placeasgeojson, AllUserAutocomplete, ActiveUserAutocomplete, MunicipalityAutocomplete, TerritoryAutocomplete, TaxaAutocomplete

urlpatterns = [
    # url(r'^place.geojson$',
    #     login_required(GeoJSONLayerView.as_view(model=Place, properties=('name', 'pk', 'municipality'), simplify=0.5)),
    #     name='geodata_place'),
    url(r'^municipality.geojson$', login_required(
        GeoJSONLayerView.as_view(model=Municipality, precision=3, simplify=0.5, properties=('name', 'code'))),
        name='geodata_municipality'),
    url(r'^territory.geojson$', login_required(
        GeoJSONLayerView.as_view(model=Territory, precision=3, simplify=0.5, properties=('name', 'code'))),
        name='geodata_territory'),
    url(r'^place.geojson$', placeasgeojson, name='geodata_place'),
    url(
        r'^active-user-autocomplete/$',
        ActiveUserAutocomplete.as_view(),
        name='active_user_autocomplete',
    ),
    url(
        r'^all-user-autocomplete/$',
        AllUserAutocomplete.as_view(),
        name='all_user_autocomplete',
    ),
    url(
        r'^taxa-autocomplete/$',
        TaxaAutocomplete.as_view(),
        name='taxa_autocomplete',
    ),
    url(
        r'^municipality-autocomplete/$',
        MunicipalityAutocomplete.as_view(),
        name='municipality_autocomplete',
    ),
    url(
        r'^territory-autocomplete/$',
        TerritoryAutocomplete.as_view(),
        name='territory_autocomplete',
    ),
]
