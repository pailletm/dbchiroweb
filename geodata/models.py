from django.conf import settings
from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _

from dicts.models import LandCoverCLC


# =========================================================================
# Données géographiques
# =========================================================================


class Territory(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, verbose_name='Nom', db_index=True)
    code = models.CharField(max_length=5, verbose_name='Code', db_index=True)
    db_territory = models.BooleanField(default=False)
    geom = models.MultiPolygonField(
        srid=settings.GEODATA_SRID, verbose_name='Emprise géographique', spatial_index=True)

    def __str__(self):
        return "%s ∙ %s" % (self.code, self.name)

    class Meta:
        verbose_name = _('Territoire du groupe local')
        verbose_name_plural = _('Territoires des groupes locaux')


class Municipality(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, verbose_name='Nom')
    code = models.CharField(max_length=5, verbose_name='Code')
    geom = models.MultiPolygonField(
        srid=settings.GEODATA_SRID, verbose_name='Emprise géographique',spatial_index=True)

    def __str__(self):
        return "%s %s" % (self.code[0:2], self.name)

    class Meta:
        verbose_name = _('Commune')
        verbose_name_plural = _('Communes')


class LandCover(models.Model):
    id = models.AutoField(primary_key=True)
    code = models.ForeignKey(LandCoverCLC, verbose_name=_("Code"), blank=True, null=True)
    geom = models.MultiPolygonField(srid=settings.GEODATA_SRID, verbose_name=_("Géométries"), spatial_index=True)

    def __str__(self):
        return "%s %s" % (self.code, self.code_descr.descr)

    # TODO Créer model dico CLC12


    class Meta:
        verbose_name = _('Occupation du sol')
        verbose_name_plural = _('Occupation du sol')

class Elevation(models.Model):
    name = models.CharField(max_length=100)
    rast = models.RasterField(srid=settings.GEODATA_SRID,  spatial_index=True)

    class Meta:
        verbose_name = _('Altitude')