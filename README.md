# dbChiro[web]

## Le projet

**dbChiro[web] est une base de donnée en ligne pour la collecte des données d'études des chauves-souris.**

Développée pour succéder à la base de donnée bdChironalpes du Groupe Chiroptère Rhône-Alpes, cette outil est développé avec le Framework Django associé à une base de données spatiale PostgreSQL/PostGIS. Il reprend en grande partie le modèle structurel de la base de donnée d'origine (dispositif MS Access/MapInfo).

Une démo est disponible ici: http://demo.dbchiro.org

Pour l'instant, 3 niveaux d'utilisateurs sont proposés:

* **Administrateur technique**, qui a accès à la totalité de la base de donnée;
* **Coordinateur**, le coordinateur local qui aura accès à toutes les données de son secteur, sites sensibles compris;
* **Observateur**, qui aura accès à la totalité de ses données ainsi qu'à tous les sites hors sites sensibles.

Pour les tester sur la démo:

|Compte|Identifiant|Mot de passe|
|-|-|-|-|
|**Administrateur technique**|dbadmin|DY2bCHY$Hz|
|**Coordinateur** du réseau/d'une section locale|coordinateur|Coordi#dbChiro*|
|**Observateur**|observateur|Observ#dbChiro*|

Une liste de discussion dédiée au projet a été crée (dbchiro [ chez ] framalistes.org):

* Pour s'inscrire ➙ https://framalistes.org/sympa/subscribe/dbchiro
* Pour se désinscrire ➙ https://framalistes.org/sympa/sigrequest/dbchiro

Contributeurs recherchés pour contribuer au projet:

* **HTML/CSS/Javascript** pour l'interface utilisateur de la base de données;
* **Python/Django** et **PostgreSQL/PostGIS** pour la motorisation de la base de données;
* **QGIS/PyQGIS/PyQT** pour l'outil d'exploitation des données.

## Installation

### Prérequis

A venir

### Installation et lancement du service

Sur un serveur Debian Jessie

#### Installation de GIT, Python3 et PostgreSQL/PostGIS

```bash
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
sudo apt-get install wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install git python3 python3-pip postgresql-9.6 postgresql-9.6-postgis-2.3 postgresql-9.6-postgis-2.3-scripts
```

#### Clonage du dépot dbChiro[web]
Dans cet exemple, dbchiroweb sera installé dans le dossier /var/www/demo.dbchiro.org
```bash
mkdir /var/www/demo.dbchiro.org
cd /var/www/demo.dbchiro.org
git clone https://fred.perso@framagit.org/dbchiro/dbchiroweb.git
```

#### Création d'un environnement virtuel
Dans cet exemple, dbchiroweb sera installé dans le dossier /var/www/demo.dbchiro.org

```bash
cd /var/www/demo.dbchiro.org
virtualenv -p python3 venv
source venv/bin/activate
```

#### Initialisation de dbChiro[web]
Editez le fichier settings.py pour l'adapter à votre serveur PostgreSQL/PostGIS en modifiant cette partie
```python
# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'dbchiroweb', # Indiquez ici le nom de la base de données
        'USER': 'superuser', # Changer dbadmin par l'identifiant du superutilisateur postgresql créé pour gérer la base de données
        'PASSWORD': 'password', # Changer password par le mot de passe du superutilisateur
        'HOST': '127.0.0.1',
        'PORT': '5432', # Port de la bdd
    }
}
```

Lancer l'installation des paquets python nécessaires

```bash
cd /var/www/demo.dbchiro.org/dbchiroweb
source ../venv/bin/activate
pip install -r requirements.txt
```

Générez une clé nouvelle clé secrète dans le fichier `secretkey.txt` à la racine du dossier principal.

```bash
python manage.py generate_secret_key > secretkey.txt
```

Lancez la création des bases de données et la collecte des fichiers statiques

```bash
python manage.py makemigrations sights
python manage.py migrate
python manage.py collectstatic
```

Lancez le serveur

```bash
python manage.py runserver
```

#### Paramétrage du serveur Apache.

Exemple de configuration:
```apache
<VirtualHost demo.dbchiro.org:80>
    ServerName demo.dbchiro.org
    ServerAlias www.demo.dbchiro.org
    ServerAdmin admin@dbchiro.org
    Alias /static /var/www/demo.dbchiro.org/dbchiroweb/static
    WSGIDaemonProcess dbchiro python-home=/var/www/demo.dbchiro.org/venv python-path=/var/www/demo.dbchiro.org/dbchiroweb
    WSGIProcessGroup dbchiro 
    WSGIScriptAlias / /var/www/demo.dbchiro.org/dbchiroweb/dbchiro/wsgi.py 
    <Directory /var/www/demo.dbchiro.org/dbchiroweb/dbchiro>
	<files wsgi.py>
            Require all granted
	    #Require expr %{HTTP_HOST} == "demo.dbchiro.org"
    	</files>
    </Directory>
    CustomLog /var/www/demo.dbchiro.org/custom.log combined
    ErrorLog /var/www/demo.dbchiro.org/error.log
</VirtualHost>
```
